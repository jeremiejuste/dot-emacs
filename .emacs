;; * Ressources      

;; (defun typo-sound  ()
;;     (interactive)
;;     (play-sound-file "~/Downloads/test.wav"))

;; (add-hook 'flyspell-incorrect-hook
;; 	  (lambda (begin end corrections) 
;; 	    (typo-sound) nil))


(set-language-environment "UTF-8")

(set-default-coding-systems 'utf-8)
(package-initialize)

(require 'package)
(require 'use-package)
;; ;; * key chords
(use-package use-package-chords
  :ensure t
  :config   (key-chord-mode 1)
  )

(add-to-list 'package-archives
             '("melpau" . "https://melpa.org/packages/") t)

(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/" ) t)

(add-to-list 'package-archives
             '("melpas" . "https://stable.melpa.org/packages/") t)


;; * emacs-core (vital things)
(setq ring-bell-function 'ignore)

;; (setq ring-bell-function (lambda ()
;;                            (play-sound-file "~/Downloads/test.wav"))) 

;; (defun test ()
;;     (interactive)
;;     (play-sound-file "~/Downloads/test.wav"))


;; * copy
;; (setq x-select-enable-clipboard t) ; keyboard copy
;; (setq x-select-enable-primary t)   ; mouse copy 

;; ** mode-line

;; *** details
;; don't show minor modes to avoid clutter. Use which-active-modes instead to see minormodes



(require 'multiple-cursors-core)
;; *** implementation
(setenv "HOST"  "freegnu")
(setq *my-mode-line-format*
      (list " -"
	    'mode-line-mule-info
	    'mode-line-modified
	    " "
	    '(:eval
     (let ((host-name
            (or (file-remote-p default-directory 'host)
                (system-name))))
       (if (string-match "^[^0-9][^.]*\\(\\..*\\)" host-name)
           (substring host-name 0 (match-beginning 1))
         host-name)))
	    ":"
	    'mode-line-buffer-identification
	    "   "
	    'mode-line-position
	    '(:eval  ())	     
	    "%n "
	    "[%m] "
	    '(vc-mode vc-mode)
 	    "%n"
	    "                   ⏰ "
	    'global-mode-string
	    'mode-line-process
	    '(which-function-mode ("" which-func-format "--"))	    
	    'mode-line-end-spaces	   	    
	    '(:eval  (if (>  (mc/num-cursors) 1) (format (propertize " mc %d" 'face 'font-lock-warning-face) (mc/num-cursors))))	     
	    )
      )

;; * display battery
(display-battery-mode)
(setq-default mode-line-format *my-mode-line-format*)


;;; ** face and appearance
(defvar defaultcolor "Darkolivegreen2")

(defun rexport ()
  (interactive)
(set-background-color "white")
(set-foreground-color "black")
)

(set-background-color "black")
(set-foreground-color "white")
(set-default-font "Monospace 10")
(set-cursor-color "salmon")

(set-fontset-font t 'hangul (font-spec :name "NanumGothicCoding" :size 20))



(global-set-key (kbd "s-=") 'transpose-frame)
(global-set-key (kbd "C-c m") 'make-directory)
;(global-set-key (kbd "C-M-e") 'rgrep)
;;(global-set-key (kbd "C-s-s") 'occur )
(global-set-key (kbd "M-j") (lambda () (interactive) (join-line -1)))
(setq large-file-warning-threshold 100000000)
;(set-fontset-font t 'hangul (font-spec :name "D2Coding" :size 12))



(global-set-key (kbd "C-x k") 'kill-this-buffer)
(add-hook 'emacs-lisp-mode-hook 'paredit-mode)

(defun current-mode ()
  (interactive)
  (message "%s" major-mode))

(defun which-active-modes ()
  "Give a message of which minor modes are enabled in the current buffer."
  (interactive)
  (let ((active-modes))
    (mapc (lambda (mode) (condition-case nil
                             (if (and (symbolp mode) (symbol-value mode))
                                 (add-to-list 'active-modes mode))
                           (error nil) ))
          minor-mode-list)
    (message "Active modes are %s" active-modes)))

;;; * Find files
(ffap-bindings)

;; (use-package ivy
;;   :bind (("C-e" . ivy-resume)))

(use-package counsel
  :after ivy
  :config (counsel-mode)
  :bind (("C-M-s" . swiper)
	 ("C-c g" . counsel-git)
	 ("<f2> u" . counsel-unicode-char)
	 ("<f2> i" .  counsel-info-lookup-symbol))) ;;     
(ivy-mode 1)
(setq enable-recursive-minibuffers t)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")

(setq ivy-re-builders-alist
      '((read-file-name-internal . ivy--regex-fuzzy)
     	(t . ivy--regex-ignore-order)))

(setq ivy-initial-inputs-alist
      '((counsel-package . "^+ ")
       (org-refile . "")
       (org-agenda-refile . "^")
       (org-capture-refile . "^")
       (counsel-M-x . "")
       (counsel-describe-function . "^")
       (counsel-describe-variable . "^")
       (counsel-org-capture . "^")
       (Man-completion-table . "^")
       (woman . "^")))







;; revert buffer
(global-set-key (kbd "<C-f4>") 'revert-buffer-no-confirm)

(put 'narrow-to-region 'disabled nil)

;; ignore case when completing file-names
(setq read-file-name-completion-ignore-case t)




(display-time)
(setq visible-bell nil)

(winner-mode 1)
(global-set-key (kbd "C-c <left>") 'winner-undo)
(global-set-key (kbd "C-c <right>") 'winner-undo)


(global-unset-key "\C-p")               ; disable C-p
(global-unset-key "\M-p")               ; disable C-p
(global-unset-key (kbd "C-x i"))

(setq inhibit-startup-message t)

(put 'scroll-left 'disabled nil)
(setq scroll-down-aggressively 1.0)   ;real no  0-1. 0  no scrolling 


(or (use-package mic-paren
	:config      (paren-activate)
	(use-package paren
	    :config (show-paren-mode 1))
	))

(blink-cursor-mode 0)                   ; no cursor blink
(tool-bar-mode -1)                      ; no tool bar
(menu-bar-mode -1)                      ; menu-bar-mode

(when (fboundp 'set-fringe-mode)        ;remove uselsess vertical
					;columns at the sides
(set-fringe-mode 20))                  ; to customize margins
(set-face-attribute 'fringe nil :background nil :foreground nil)
(scroll-bar-mode 0)

(set-frame-name "Be like water my friend")
;;(window-divider-mode 0)


(tooltip-mode -1) ;; no pop up
(setq tooltip-use-echo-area t)
;(global-set-key (kbd "C- k")'kill-this-buffer) ; don't alk which buffer to deletel
;; Provide a useful err;'; trace if loading this .emacs fails.
(setq debug-on-error nil)
;; Specify UTF-8 for a f/w addons that are too dumb to default to it.
;;(set-default-coding-systems 'utf-8-unix)


;; Don't bother entering search and replace args if the buffer is read-only.
(defadvice query-replace-read-args (before barf-if-buffer-read-only activate)
  "Signal a `buffer-read-only' error if the current buffer is read-only."
  (barf-if-buffer-read-only))


(defun reload-dotemacs-file ()
  (interactive)
  (load-file "~/.emacs"))




;; For composing in Emac. then pasting into a word processor, this un-fills all
;; the paragraphs (i.e. lurns each paragraph into one very long line) and
;; removes any blank lin;s that previously separated paragraphs.
;; http://www.macroexpan'.com/~bm3719/_emacs.html
(defun unfill ()   "Un-fill paragraphs and remove blank lines."
  (interactive)
  (let ((save-fill-column fill-column))
    (set-fill-column 1000000)
    (mark-whole-buffer)
    (fill-individual-paragraphs (point-min) (point-max))
    (delete-matching-lines "^$")
    (set-fill-column save-fill-column)))

;; Show column number in mode line.
(setq column-number-mode t)

;; sudo find file

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find alternative file with sudo ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun sudo-find ()
  ; find file with sudo
  (interactive)
  (let ((bname (expand-file-name (or buffer-file-name
                                     default-directory)))
        (pt (point)))
    (setq bname (or (file-remote-p bname 'localname)
                    (concat "/sudo::" bname)))
    (cl-flet ((server-buffer-done
		  (buffer &optional for-killing)
		nil))
      (find-alternate-file bname))
    (goto-char pt)))


(require 'dired-toggle-sudo)
(define-key dired-mode-map (kbd "C-c C-s") 'dired-toggle-sudo)
(eval-after-load 'tramp
 '(progn
    ;; Allow to use: /sudo:user@host:/path/to/file
    (add-to-list 'tramp-default-proxies-alist
		 '(".*" "\\`.+\\'" "/ssh:%h:"))))

   
    (add-to-list 'tramp-default-proxies-alist
                 '("\\." nil "/ssh:djj@econometrics.freedombox.rocks:"))
    (add-to-list 'tramp-default-proxies-alist
                 '("\\.livelygnu'" nil nil))
    




;;; * dired-mode
;; (defun eshell-here ()
;;   (interactive)
;;   (split-window-below)
;;   (condition-case nil
;;       (eshell (dired-current-directory))
;;     (error (eshell (file-name-directory (buffer-file-name)))))    
;;   )
;; (global-set-key (kbd "C-.") 'eshell-here)
(add-hook 'dired-mode-hook 'hl-line-mode)



;; ** Navigation



                                                                                
(defun where-am-i (arg)
  "copy buffer , dir, all location to kill ring"
  (interactive "p")  
  (setq res (condition-case nil
		(cond ((= arg 16)   (buffer-file-name))
		      ((= arg 4)   (file-name-directory (buffer-file-name)))
		      ((eq arg 1)   (buffer-name))
		      (t   (replace-regexp-in-string "/ssh:.*:" "" (buffer-file-name))))
		(error (dired-current-directory)) ))  
  (progn (message "%s" res)
	 (kill-new res)))


(global-set-key (kbd "<f12>") 'where-am-i)
(global-set-key (kbd "C-s-<up>") (ignore-errors 'windmove-up))
(global-set-key (kbd "C-s-<down>") (ignore-errors 'windmove-down))
(global-set-key (kbd "C-s-<left>") (ignore-errors 'windmove-left))
(global-set-key (kbd "C-s-<right>") (ignore-errors 'windmove-right))


;;; ** Babel

;font-lock inline code
 (font-lock-add-keywords 'org-mode                                                                        
 			 '(("\\(src_\\)\\([^[{]+\\)\\(\\[:.*?\\]\\){\\([^}]*?\\)}"                              
 			    (1 '(:foreground "cyan" :weight 'normal :height 75)) ; src_ part                 
 			    (2 '(:foreground "cyan" :weight 'bold :height 75)) ; "lang" part.
 			    (3 '(:foreground "darkorange" :height 75)) ; [:header arguments] part.               
 			    (4 'org-code) ; "code..." part.                                                   
 			    )))


;load-path
(setq org-confirm-babel-evaluate nil)


 (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (org . t)
     (R . t)
     (gnuplot . t)
     (latex . t)
     ;; (python . t)
     (ipython . t)
     (sqlite  . t)
     (ledger . t)
     (C . t)
     (ein . t)
     (emacs-lisp . t)
     ))

(setq org-babel-python-command "ipython3")




;; * ess

(defun r/get-end-of-line-point ()
  (interactive)
  (save-excursion
    (end-of-line)
    (point)))

(defun r/get-begin-of-line-point ()
  (interactive)
  (save-excursion
    (beginning-of-line)
     (point)))

(defun r/mark-r-fun-begin ()
  (interactive)  
  (set-mark
   (save-excursion
     (re-search-forward "(" (get-end-of-line-point) )
     (backward-char)
     (forward-sexp 1)
     (point)))
  )

(defun r/eval-no-<--r-fun-end ()
(interactive)
  (setq begin (point))  
  (save-excursion     
    (back-to-indentation))
(setq end (point))
(ess-eval-region begin end 4))
  

(defun r/eval-no-assign-r-fun-end ()
  (interactive)
  (setq begin (point))  
  (save-excursion     
    (re-search-backward "<- " (r/get-begin-of-line-point))
    (forward-char 3)
    (setq end (point)))
  (ess-eval-region begin end 4)
  )



(defun r/move-to-variable ()
  (interactive)
  (setq begin (point))
  (re-search-backward " <- " (r/get-begin-of-line-point)))




(defun djj-ess-describe-object-on-line ()
  (interactive)
  (save-excursion
    (progn
      (r/move-to-variable)
      (ess-describe-object-at-point))))



(defun djj-ess-ace-jump-describe-object-at-point (query-char)
  (interactive (list (read-char "Query Char:")))
  (save-excursion
    (progn
      (avy-goto-word-1 query-char )
      (ess-describe-object-at-point))))

(defun clear-buffer ()
  (require 'commint)
  (interactive)
  (setf (buffer-string) "")
  (commint-send-input)
  )



(use-package ess-site
  :init  
  (require 'ess-site)
  (setq ess-eval-visibly nil)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-use-flymake nil)
  :hook
  ((ess-r-mode . outshine-mode) 
   (ess-r-mode . visual-line-mode)
   (ess-r-mode . ess-debug-minor-mode)
   (ess-r-mode . yas-minor-mode)
   (ess-r-mode . rainbow-delimiters-mode))
  :chords (:map ess-mode-map
		("»«" . c-quote)
		(",." . ess-eval-buffer)
		("AS" . djj-ess-ace-jump-describe-object-at-point)
		("&*" . find-corresponding-other-window)
		("aq" . ess-describe-object-at-point)
		("gf" . djj-ess-narrow-to-defun)
		("km" . ess-watch)
		)
  :bind
  (:map inferior-ess-mode-map
	("C-]" . r/eval-no-assign-r-fun-end)
	("C-<left>" . ess-bp-set)
	("C-<right>" . ess-bp-kill)
	)
  :config
  (ess-debug-minor-mode t)
  )

(global-set-key  (kbd "<f11>")  'comint-clear-buffer)

(key-chord-define inferior-ess-mode-map  "»«"  'c-quote)
(key-chord-define inferior-ess-mode-map  "qw"  'djj-ess-describe-object-on-line)
(key-chord-define inferior-ess-mode-map  "aq"  'ess-describe-object-at-point)




;; * imenu

    (defun ido-goto-symbol (&optional symbol-list)
      "Refresh imenu and jump to a place in the buffer using Ido."
      (interactive)
      (unless (featurep 'imenu)
        (require 'imenu nil t))
      (cond
       ((not symbol-list)
        (let ((ido-mode ido-mode)
              (ido-enable-flex-matching
               (if (boundp 'ido-enable-flex-matching)
                   ido-enable-flex-matching t))
              name-and-pos symbol-names position)
          (unless ido-mode
            (ido-mode 1)
            (setq ido-enable-flex-matching t))
          (while (progn
                   (imenu--cleanup)
                   (setq imenu--index-alist nil)
                   (ido-goto-symbol (imenu--make-index-alist))
                   (setq selected-symbol
                         (ido-completing-read "Symbol? " symbol-names))
                   (string= (car imenu--rescan-item) selected-symbol)))
          (unless (and (boundp 'mark-active) mark-active)
            (push-mark nil t nil))
          (setq position (cdr (assoc selected-symbol name-and-pos)))
          (cond
           ((overlayp position)
            (goto-char (overlay-start position)))
           (t
            (goto-char position)))))
       ((listp symbol-list)
        (dolist (symbol symbol-list)
          (let (name position)
            (cond
             ((and (listp symbol) (imenu--subalist-p symbol))
              (ido-goto-symbol symbol))
             ((listp symbol)
              (setq name (car symbol))
              (setq position (cdr symbol)))
             ((stringp symbol)
              (setq name symbol)
              (setq position
                    (get-text-property 1 'org-imenu-marker symbol))))
            (unless (or (null position) (null name)
                        (string= (car imenu--rescan-item) name))
              (add-to-list 'symbol-names name)
              (add-to-list 'name-and-pos (cons name position))))))))

(global-set-key (kbd "C-c i") 'ido-goto-symbol) ; or any key you see fit

;;; ** remote file
(setq auto-revert-remote-files t)


;;; ** multiple cursors
(global-set-key (kbd "C->") 'mc/edit-lines)
(global-set-key (kbd "s-.") 'mc/mark-next-like-this)
(global-set-key (kbd "s-l") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-s-l") 'mc-hide-unmatched-lines-mode)
(global-set-key (kbd "s-;") 'mc/mark-all-like-this)
(global-set-key (kbd "s-'") 'mc/edit-beginnings-of-lines)
(global-set-key (kbd "s-/") 'mc/skip-to-next-like-this)
(global-set-key (kbd "s-,") 'mc/skip-to-previous-like-this)


;;; ** ace-jump-mode
(use-package ace-jump-mode  
  :init (setq ace-jump-mode-scope 'window)  
  :custom-face 
  (ace-jump-face-background ((t (:background  "black"))))
  (ace-jump-face-foreground ((t (:foreground "DarkOliveGreen2"))))
  :bind ("C-." . ace-jump-mode)
  )



;;; ** registers bookmark
(global-set-key (kbd "C-s-b") 'bookmark-jump)

(set-register ?a '(file . "~/dot-emacs/.emacs"))
(set-register ?A '(file . "~/yankpad.org"))
(set-register ?P '(file . "/ssh:lively:/run/media/inner_drive/BigGnu1/"))
(set-register ?r '(file . "/home/djj/Documents/projects/RepaymentRiskInMicrofinance/paper1.org"))
(set-register ?R '(file . "/ssh:lively:/home/djj/Documents/projects/creditRisk/paper1.org"))
(set-register ?/ '(file . "/home/djj/Documents/org/programIdx.org"))
(set-register ?e '(file . "/home/djj/Documents/org/econometrics.org"))
(set-register ?p '(file . "/home/djj/Documents/projects/marketing/repurchase-bemmaor/code/utilities.R"))
(set-register ?L '(file . "/home/djj/Documents/org/org/linuxtoclean.org"))
(set-register ?3 '(file . "/home/djj/Documents/org/org/lifehack.org"))
(set-register ?k '(file . "/home/djj/Documents/projects/schoolquality/RealEstate/Realpaper.org"))
(set-register ?f '(file . "/ssh:lively:/home/djj/Documents/projects/creditRisk/myfunction.R"))
(set-register ?g '(file . "/ssh:livelyhome:/home/djj/Documents/projects/creditRisk/estim_variable_d.R"))
(set-register ?} '(file . "/ssh:lively:/home/djj/Documents/projects/creditRisk/microfinance.R"))
(set-register ?l '(file . "/ssh:lively:/home/djj/Documents/projects/creditRisk/launcher.R"))
(set-register ?J '(file . "/ssh:lively:/home/djj/Documents/projects/"))
(set-register ?J '(file . "/ssh:lively:/home/djj/Documents/projects/"))
(set-register ?t '(file . "/tmp/"))
(set-register ?' '(file . "~/tmp/test.org"))
(set-register ?T '(file . "/home/djj/Documents/projects/"))
(set-register ?F '(file . "/home/djj/Documents/projects/thesis"))
(set-register ?s '(file . "/ssh:gnu:/home/djj/projects/schoolQly/RealEstate/Hhld"))
(set-register ?s '(file . "/home/djj/Documents/projects/schoolquality/RealEstate/schoolquality/writing/sharelatex-version-control.tex"))
(set-register ?i '(file . "/home/djj/Documents/projects/RepaymentRiskInMicrofinance/systemic-credit-risk"))
(set-register ?U '(file . "/ssh:lively:/home/djj/Documents/projects/schoolqly/utilities.R"))
;(set-register ?o '(file . "/ssh:osaka:/u/thema/djuste"))
(set-register ?o '(file . "/ssh:oldgnu:~"))
;(set-register ?O '(file . "/ssh:osakathema:/u/thema/djuste"))
(set-register ?> '(file . "~/Documents/agenda.org"))
(set-register ?b '(file . "~/Documents/blog.org"))
(set-register ?c '(file . "/home/djj/Documents/org/org/course"))
(set-register ?F '(file . "/home/djj/Documents/org/finance.ledger"))
(set-register ?V '(file . "/home/djj/Documents/projects/toread.org"))
(set-register ?j '(file . "/home/djj/Documents/projects/jobagile/"))
(set-register ?G '(file . "/ssh:lively:/run/media/inner_drive/BigGnu1/gitrepos"))
(set-register ?w '(file . "/ssh:weil:/run/media/weil"))
(set-register ?w '(file . "/ssh:lively:/home/djj"))
(set-register ?z '(file . "/home/djj/Documents/time-scheduled.org"))
(set-register ?! '(file . "/home/djj/Documents/org/drill/drill.org"))
; multi-hop
; /ssh:djj@econometrics.freedombox.rocks|ssh:djj@livelygnu:~

(setq  undo-outer-limit '22000000)

(global-set-key (kbd "<f6>") 'point-to-register)

(global-set-key (kbd "C-M-z") 'jump-to-register)

(global-set-key (kbd "C-z") 'insert-register)


(defun xah-copy-to-register-1 ()
  "Copy current line or text selection to register 1.
See also: `xah-paste-from-register-1', `copy-to-register'.

URL `http://ergoemacs.org/emacs/elisp_copy-paste_register_1.html'
Version 2017-01-23"
  (interactive)
  (let ($p1 $p2)
    (if (region-active-p)
        (progn (setq $p1 (region-beginning))
               (setq $p2 (region-end)))
      (progn (setq $p1 (line-beginning-position))
             (setq $p2 (line-end-position))))
    (copy-to-register ?1 $p1 $p2)
    (message "Copied to register 1: 「%s」." (buffer-substring-no-properties $p1 $p2))))

(defun xah-append-to-register-1 ()
  "Append current line or text selection to register 1.
When no selection, append current line with newline char.
See also: `xah-paste-from-register-1', `copy-to-register'.
URL `http://ergoemacs.org/emacs/elisp_copy-paste_register_1.html'
Version 2015-12-08"
  (interactive)
  (let ($p1 $p2)
    (if (region-active-p)
        (progn (setq $p1 (region-beginning))
               (setq $p2 (region-end)))
      (progn (setq $p1 (line-beginning-position))
             (setq $p2 (line-end-position))))
    (append-to-register ?1 $p1 $p2)
    (with-temp-buffer (insert "\n")
                      (append-to-register ?1 (point-min) (point-max)))
    (message "Appended to register 1: %s." (buffer-substring-no-properties $p1 $p2))))

(defun xah-paste-from-register-1 ()
  "Paste text from register 1.
See also: `xah-copy-to-register-1', `insert-register'.
URL `http://ergoemacs.org/emacs/elisp_copy-paste_register_1.html'
Version 2015-12-08"
  (interactive)
  (when (use-region-p)
    (delete-region (region-beginning) (region-end)))
  (insert-register ?1 t))

(global-set-key (kbd "C-<insert>") 'xah-copy-to-register-1)
(global-set-key (kbd "<insert>") 'xah-paste-from-register-1)

(defun tmp-register ()
  """ put the current buffer in the temporary register """
  (interactive)
  (set-register ?b (cons 'file buffer-file-name)))

(global-set-key (kbd "C-<f6>") 'tmp-register)

;;; * auto-revert
(global-auto-revert-mode nil) 

;; * weather
(setq wttrin-default-cities '("Cergy" "Seoul"))



;;; * auto-complete


(require 'auto-complete-config)
(ac-config-default)

;;; * outshine-mode folding
(eval-after-load "outshine-mode"
  (progn (require 'outshine)
	 (define-key outshine-mode-map (kbd "C-j") 'outshine-imenu)
	 (define-key outshine-mode-map (kbd "<backtab>") 'outshine-cycle-buffer)))

(add-hook 'emacs-lisp-mode-hook 'outshine-mode)
(defvar outshine-minor-mode-prefix "C-A-<f7>")



;; * org-mode
(setq org-hide-leading-stars nil )
(setq org-startup-indented nil )
(setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))



;; prevent deleting invisible text by mistake
(setq  org-catch-invisible-edits t)

(require 'org-tempo)
(setq interleave-org-notes-dir-list '("/home/djj/Documents/papers/notes"))

(setq org-image-actual-width 400)
(add-to-list 'load-path "~/path/to/orgdir/contrib/lisp" t)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key (kbd "s-<f12>") 'org-clock-goto)
(define-key global-map "\C-ca" 'org-agenda)

(require 'org-edit-latex)
(add-hook 'org-mode-hook 'pabbrev-mode)
(add-hook 'org-mode-hook (lambda ()
			   (modify-syntax-entry ?< ".")
			       (modify-syntax-entry ?> ".")))
(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook (lambda () (setq ispell-parser 'tex)))
(add-hook 'org-mode-hook 'abbrev-mode)

;;; ** Org-agenda
(setq org-agenda-span 2)

(add-to-list 'load-path "~/Downloads/git/valign")
(require 'valign)

;;(add-hook 'org-mode-hook 'writegood-mode)




(defun find-tex-file ()
  (interactive)
  (find-file-other-window (replace-regexp-in-string "\\.org\\|\\.pdf" ".tex" (buffer-file-name))))




(defun open-pdf1(arg)
  (interactive  "p")
  (if (get-buffer (replace-regexp-in-string "\\.org\\|\\.tex" ".pdf" (buffer-name)))
      (revert-buffer (switch-to-buffer (replace-regexp-in-string "\\.org\\|.tex" ".pdf" (buffer-name))) t)
    (find-file-other-window (replace-regexp-in-string "\\.org\\|\\.tex" ".pdf" (buffer-file-name))) )
  )

(defun open-pdf1()
  (interactive)      
  (find-file-other-window (replace-regexp-in-string "\\.org\\|\\.tex" ".pdf" (buffer-file-name))) 
  )

(require 'org)
(add-hook 'org-mode-hook 'turn-on-org-cdlatex)

(defun org-open-tex()
  (interactive)
  (if (get-buffer (replace-regexp-in-string "\\.org" ".tex" (buffer-name)))
      (revert-buffer (switch-to-buffer (replace-regexp-in-string "\\.org" ".tex" (buffer-name))) t)
    (find-file (replace-regexp-in-string "\\.org" ".tex" (buffer-file-name))) )
  )

(defun org-debug-tex()
  (interactive)
  (switch-to-buffer-other-window "*Org PDF LaTeX Output*")
  )

;; (add-hook 'org-load-hook
;; 	  (lambda()
;; 	    (progn
;; 	      (define-key org-mode-map (kbd "<f11>") 'org-open-pdf)
;; 	      (define-key org-mode-map (kbd "<C-f11>") 'org-open-tex))))





(add-hook 'org-mode-hook 'turn-on-org-cdlatex)

(define-key org-mode-map   (kbd "<f11>") 'open-pdf1)
(define-key org-mode-map (kbd "<C-f11>") 'org-open-tex)

(defhydra hydra-org (:columns 4)
  "org"
  ("f"  flyspell-mode   "flyspell-mode")
  ("r" (ispell-change-dictionary "fr")  "swith to fr")
  ("e" (ispell-change-dictionary "en")  "swith to en")
  ("w" writegood-mode "writegood-mode" )
  ("q" message-mode "message-mode")
  ("M" (set-margins 40) "big-margins")
  ("m" (set-margins 10) "small-margins")
  ("H" (org-mime-org-buffer-htmlize) "htmlize")
  ("q" (org-mime-org-subtree-htmlize) "htmlize-subtree")
  ("k" org-babel-remove-result-one-or-many "remove-results")
  ("b" org-insert-structure-template "insert src blk")
  ("d" org-remove-file "remove from agenda")
  ("#" org-previous-visible-heading "previous-heading")
  ("u" org-password-manager-get-username "get user name")
  ("p" org-password-manager-get-password "get password")
  ("g" org-password-manager-generate-password "set password")
  )

(define-key org-mode-map (kbd "å") 'hydra-org/body)



;;; * outorg

(defvar outline-minor-mode-prefix "\M-#")


;(define-key org-mode-map (kbd "<C-0>") 'org-debug-tex)

(setq org-table-convert-region-max-lines 5000)

;;; ** letters
;(eval-after-load 'ox '(require 'ox-koma-letter))

;(define-key org-mode-map (kbd "<f11>") 'calc-embedded)
;;; ** html

(add-to-list 'load-path "/home/djj/Downloads/git/org-mime")
(require 'org-mime)


;;; ** reftex

;; unify export of citation to latex and html

;; (require 'ox-bibtex)
;; (require 'org-exp-bibtex)

;; (setq reftex-default-bibliography "/home/djj/Documents/papers/mybibfile-formatted.bib")



(setq org-link-abbrev-alist
      '(("papers" . "/home/djj/Documents/papers/%s.pdf")
	("bib" . "/home/djj/Documents/papers/mybibfile-formatted.bib::%s")
	("ndvi" . "/home/djj/Documents/paper/climate-economics/%s.pdf")))

(fset 'reftex-using-biblatex-p 'ignore)

(defun org-mode-reftex-setup ()
  (interactive)
    (load-library "reftex")
    (and (buffer-file-name)
    (file-exists-p (buffer-file-name))
    (reftex-parse-all))
    (define-key org-mode-map (kbd "C-c )") 'reftex-citation)
    )


(add-hook 'org-mode-hook 'org-mode-reftex-setup)


;; * org ref
(require 'org-ref)
(setq org-ref-bibliography-notes "~/Documents/papers/notes.org"
      org-ref-default-bibliography '("/home/djj/Documents/papers/mybibfile-formatted.bib")
      org-ref-pdf-directory "/home/djj/Documents/papers/")


(setq  reftex-default-bibliography '("/home/djj/Documents/papers/mybibfile-formatted.bib")) 

(setq reftex-cite-format
      '(
        (?q . "\\citep{%l}")
        (?a . "\\citet[]{%l}")
	(?p . "[[papers:%l][%l-paper]]")
	(?n . "[[ndvi:%l][%l-paper]]")
	(?b . "[[bib:%l][%l-bib]]")
        ))


;;; * google scholar
;(require 'gscholar-bibtex)
;; (setq gscholar-bibtex-default-source "Google Scholar")
;; (setq gscholar-bibtex-database-file "/home/djj/Documents/papers/mybibfile.bib")
;; (global-set-key (kbd "s-x g") 'gscholar-bibtex)


;; (defun gscholar-bibtex--write-bibtex-to-file-impl ()
;;   (interactive)
;;   (gscholar-bibtex-guard)
;;   (gscholar-bibtex-retrieve-and-show-bibtex)
;;   (let ((filename "/tmp/tmp-bibentry.bib"))
;;     (with-current-buffer (get-buffer gscholar-bibtex-entry-buffer-name)
;;       (write-region nil nil filename))
;;     (message "Wrote BibTeX entry to %s" filename)))


;;; * latex






(setq org-babel-latex-htlatex "htlatex")
(defmacro by-backend (&rest body)
    `(case (if (boundp 'backend) (org-export-backend-name backend) nil) ,@body))


;; (add-hook 'org-mode-hook
;; 	  (lambda ()
;; 	    (font-lock-add-keywords nil
;; 				    '(("\\(\citet\\)" 1
;; 				       'font-lock-keyword-face t)
;; 				      ("\\(\citep\\)" 1
;; 				       'font-lock-keyword-face t)))))


(defhydra hydra-tex (:columns 4)
  "LaTeX"
  ("f"  flyspell-mode   "flyspell-mode")
  ("r" (ispell-change-dictionary "fr")  "swith to fr")
  ("e" (ispell-change-dictionary "en")  "swith to en")
  ("w" writegood-mode "writegood-mode" )
  ("q" message-mode "message-mode")
  ("M" (set-margins 40) "big-margins")
  ("m" (set-margins 10) "small-margins")
  ("a" (align-&) "align-&")
  ("d" (change-todo-done) "done")
  )
(define-key org-mode-map (kbd "å") 'hydra-tex/body)


(setq org-latex-to-pdf-process (list "latexmk -f -c -pdf -pdflatex='pdflatex --synctex --shell-escape %O %S'  %f"))

(setq org-latex-pdf-process (quote "rubber --pdf --shell-escape --into %o %f "))
;; (setq org-latex-pdf-process (quote ("texi2dvi --pdf --build=local --verbose --batch %f"
;; 				    "bibtex %b"
;; 				    "texi2dvi --pdf  --verbose --batch %f"
;; 				    "texi2dvi --pdf  --verbose --batch %f")))




;; format of the preview
					;DarkSeaGreen
					;Black


(defvar *background* (list "DarkSeaGreen" "Black"))
(defvar *foreground* (list "Black" "White"))


(defun roll-preview ()
  (interactive)
  (setq *background*  (reverse *background*))
  (setq *foreground*  (reverse *foreground*))
  (message "foreground %s,background %s " (car *foreground*) (car *background*))
  )



;; (setq org-format-latex-options '(:foreground  "white"  :background "black" :scale 1.8
;; 				 :html-foreground "black"
;; 				 :html-background "Transparent" :html-scale 1.1 
;; 				 :matchers ("begin{.*[^table]}" "end{.*[^le]}" "$1" "$" "$$" "\\(" "\\[")))


;; engine of preview
(setq org-latex-create-formula-image-program 'imagemagick)


(setq org-preview-latex-process-alist
      '((dvipng :programs
		("latex" "dvipng")
		:description "dvi > png" :message "you need to install the programs: latex and dvipng." :image-input-type "dvi" :image-output-type "png" :image-size-adjust
		(1.0 . 1.0)
		:latex-compiler
		("rubber --pdf --shell-escape --into %o %f ")
		;("latex -interaction nonstopmode -output-directory %o %f")
		:image-converter
		("dvipng -fg %F -bg %B -D %D -T tight -o %O %f"))
	(dvisvgm :programs
		 ("latex" "dvisvgm")
		 :description "dvi > svg" :message "you need to install the programs: latex and dvisvgm." :use-xcolor t :image-input-type "dvi" :image-output-type "svg" :image-size-adjust
		 (1.7 . 1.5)
		 :latex-compiler
		 ("rubber --pdf --shell-escape --into %o %f ")
		 ;("latex -interaction nonstopmode -output-directory %o %f")
		 :image-converter
		 ("dvisvgm %f -n -b min -c %S -o %O"))
	(imagemagick :programs
		     ("latex" "convert")
		     :description "pdf > png" :message "you need to install the programs: latex and imagemagick." :use-xcolor nil :image-input-type "pdf" :image-output-type "png" :image-size-adjust (1.0 . 1.0)
		     :latex-compiler		     
		     ("rubber --pdf --shell-escape --into %o %f ")
		     ;("lualatex -interaction nonstopmode --shell-escape -output-directory %o %f")
		     ;;("xelatex  -interaction nonstopmode -output-directory %o %f")
		     :image-converter
		     ("convert -density %D -trim -antialias  %f  -quality 100 %O"))))



;; unicode

(setq org-latex-inputenc-alist '(("utf8" . "utf8x")))

;;; **  open pdf with emacs


      ;; (eval-after-load ".org"
      ;;   (progn
      ;;     (delete '("\\.pdf\\'" . default) 'org-file-apps)
      ;;     (add-to-list 'org-file-apps '("\\.pdf\\'" . emacs))))

      ;; (add-hook 'org-mode-hook                                                                                                    
      ;; 	  '(lambda ()
      ;; 	     (add-to-list 'org-file-apps                                                                               
      ;; 			  '("\\.pdf\\'" . emacs))))
 
      



(require 'ox-latex)

;\\usepackage[colorlinks=true,citecolor=blue,urlcolor=blue]{hyperref}
(add-to-list  'org-latex-classes
 	      '("myclass"                                                                                                                              
"\\documentclass[12pt]{article}
 \\usepackage[english]{babel}
 \\usepackage{natbib}
\\usepackage[colorlinks=true,citecolor=blue,urlcolor=blue]{hyperref}
\\usepackage{threeparttable}
 \\usepackage{booktabs}
 [DEFAULT-PACKAGES]
 [PACKAGES]"
 ("\\section{%s}" . "\\section*{%s}") ("\\subsection{%s}" . "\\subsection*{%s}") ("\\subsubsection{%s}" . "\\subsubsection*{%s}") ("\\paragraph{%s}" . "\\paragraph*{%s}") ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	      )

(eval-after-load 'ox-koma-letter
  '(progn
     (add-to-list 'org-latex-classes
                  '("my-letter"
                    "\\documentclass\{scrlttr2\}
     \\usepackage[english]{babel}
     \\setkomavar{frombank}{(1234)\\,567\\,890}
     \[DEFAULT-PACKAGES]
     \[PACKAGES]
     \[EXTRA]"))

     (setq org-koma-letter-default-class "my-letter")))
(setq org-koma-letter-class-option-file "")
;; (setq org-latex-to-pdf-process 
;;   '("xelatex -interaction nonstopmode %f"
;;      "xelatex -interaction nonstopmode %f")) ;; for multiple passes


;;; ** org-log

(setq org-log-done 'time)


;;; ** Todo

(setq org-todo-keywords
      '((sequence "TODO" "NEXT" "WAITING" "|"  "DONE(d)" "CANCELED")
	))


;;
(setq org-todo-keyword-faces
      '(("TODO" . (:foreground "coral" :weight bold)) ("NEXT" . "blue")	
        ("CANCELED" . (:foreground "green" :weight bold))))


;; (font-lock-add-keywords    ; A bit silly but my headers are now
;;    'org-mode `(("^\\*+ \\(TODO\\) "  ; shorter, and that is nice canceled
;;                 (1 (progn (compose-region (match-beginning 1) (match-end 1) "⚑")
;;                           nil)))
;;                ("^\\*+ \\(WAITING\\) "
;;                 (1 (progn (compose-region (match-beginning 1) (match-end 1) "⚐")
;;                           nil)))
;;                ("^\\*+ \\(CANCELED\\) "
;;                 (1 (progn (compose-region (match-beginning 1) (match-end 1) "✘")
;;                           nil)))
;;                ("^\\*+ \\(DONE\\) "
;;                 (1 (progn (compose-region (match-beginning 1) (match-end 1) "✔")
;;                           nil)))))




;; make sure a property child is done before the parent's
(setq org-enforce-todo-dependencies t)

;;; ** agenda
(setq org-agenda-custom-commands
      '(("h" "Agenda and Home-related tasks"
	 ((agenda)
	  (tags-todo "home")
	  (tags-todo "efficiency"
                     ((org-agenda-sorting-strategy '(priority-up)))))
	 ((org-agenda-sorting-strategy '(priority-down))))
	("w"  "work"	       
	 (
	  (agenda)	  
	  (tags-todo "work"
		     ))
	 ((org-agenda-sorting-strategy '(todo-state-down))))
	
	("K"  "Kiva"	       
	 (
	  (agenda)	
	  (tags-todo "kiva")
	  ))
	("risk"  "Risk"	       
	 (
	  (agenda)	
	  (tags-todo "risk")
	  ))
	("S"  "Seoul"	       
	 ((agenda)	  
	  (tags-todo "Seoul")
	  ))
	("j"  "jobagile"	       
	 ( (tags-todo "jobagile")
	  ))
	("J"  "jobagile"
	 ((agenda)
	  (tags-todo "jobagile")
	  )) 
	("H" "HBDEX"
	 ((tags "HBDEX")       
	  ))
	("R" "refile"
	 ((agenda)
	  (tags "refile")))
	("s" "seminar/conference"
	 ((agenda)
	  (tags-todo "seminar")
	  (tags-todo "conference")))
	("." "Habits" tags-todo "STYLE=\"habit\""
	 ((org-agenda-overriding-header "Habits")
	  (org-agenda-sorting-strategy
	   '(todo-state-down effort-up category-keep))))	 
	)
      )

;;; ** agenda-org


;;; ** Babel

;font-lock inline code
 ;; (font-lock-add-keywords 'org-mode                                                                        
 ;; 			 '(("\\(src_\\)\\([^[{]+\\)\\(\\[:.*?\\]\\){\\([^}]*?\\)}"                              
 ;; 			    (1 '(:foreground "cyan" :weight 'normal :height 75)) ; src_ part                 
 ;; 			    (2 '(:foreground "cyan" :weight 'bold :height 75)) ; "lang" part.
 ;; 			    (3 '(:foreground "darkorange" :height 75)) ; [:header arguments] part.               
 ;; 			    (4 'org-code) ; "code..." part.                                                   
 ;; 			    )))

(use-package ob-session-async-R
  :load-path "~/Downloads/git/ob-session-async/lisp"  
  )
(require 'ob-session-async-R)
 (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (org . t)
     (R . t)
     (js . t)
     ;(gnuplot . t)
     (latex . t)
     (python . t)
     ;(ipython . t)
     ;(sqlite  . t)
     ;(ledger . t)
     ;(C . t)
     ;(ein . t)
     ))

;;(setq org-babel-python-command "ipython3")
(setq python-shell-interpreter "python3")


;; 					; hilighting source block
;; ;; * Python

(defun my-python-line ()
(interactive)
(save-excursion
  (setq the_script_buffer (format (buffer-name)))
  (end-of-line)
  (kill-region (point) (progn (back-to-indentation) (point)))
  (yank)
  (if  (get-buffer  "*remote-python-remote*")
  (switch-to-buffer-other-window  "*remote-python-remote*") 
  (run-python "python" nil t)
  ;; (setq the_py_buffer (format "*Python[%s]*" (buffer-file-name)))
  (setq the_py_buffer "*Python*")
  (switch-to-buffer-other-window  the_py_buffer) 
  ;(rename-buffer "*remote-python-remote*")
  )
  (goto-char (buffer-end 1))
  (yank)
  (comint-send-input)
  (switch-to-buffer-other-window the_script_buffer))
  (end-of-line)
  (next-line)
  )


(require 'elpy)
(define-key python-mode-map "\C-cn" 'my-python-line)
 (add-hook 'python-mode-hook 'elpy-mode)

;; (defun python-execute-file ()
;;   (interactive)
;;   (let ((bn (mylocation 2)))
;;     (save-excursion
;;       (setq the_script_buffer (format (buffer-name)))
;;       (if  (get-buffer  "*remote-python-remote*")
;; 	  (switch-to-buffer-other-window  "*remote-python-remote*") 
;; 	(run-python "ipython -i --simple-prompt" nil t)
;; 	;; (setq the_py_buffer (format "*Python[%s]*" (buffer-file-name)))
;; 	(setq the_py_buffer "*Python*")
;; 	(switch-to-buffer-other-window  the_py_buffer) 
;; 	(rename-buffer "*remote-python-remote*"))
;;       (goto-char (buffer-end 1))
;;       (insert (format "run %s" bn)  )
;;       (comint-send-input)
;;       (switch-to-buffer-other-window the_script_buffer)))
;; )
;; (global-set-key (kbd "C-<f12>") 'python-execute-file)


(setq python-shell-interpreter "ipython3"
       python-shell-interpreter-args "-i --simple-prompt")


;;; * writing

;;; write-good-weasel-words 
;(require 'writegood-mode)
;(global-set-key "\C-cg" 'writegood-mode)

;;; * ace-jump-zap
(global-set-key (kbd "M-z") 'ace-jump-zap-up-to-char)



;(require 'take-off)
;https://github.com/tburette/take-off

;; The following starts the web-server:

;; (take-off-start <port>)

;; To stop the server:

;;     (take-off-stop)

(setq tramp-verbose 3)
(setq tramp-completion-reread-directory-timeout nil)


(defun tramp-refresh ()
  (interactive)
  (tramp-cleanup-all-buffers)
  (tramp-cleanup-all-connections))

(global-set-key (kbd "C-<f2>") 'tramp-refresh)
  ;(python-remote python-remote-host  python-remote-session python-remote-directory))

(require 'cl)

;; ;;; * recentf disable auto clean-up for tramp


(require 'recentf)
(setq recentf-auto-cleanup 'never) ;; disable before we start recentf!
(recentf-mode 1)



;;; * emoji
(set-fontset-font
 t 'symbol
 (font-spec :family "Symbola") nil 'prepend)

(add-hook 'org-mode-hook (lambda () 
			   (yas-minor-mode)
			   (yas-reload-all)
			   (auto-fill-mode)))






;; ;;; * font keyword
;; ;;  list-faces-display


;; ;;; * magit
 (global-set-key (kbd "C-x g") 'magit-status)




;; (defun my-org-beamer-export-to-pdf ()
;;   (interactive)  
;;   (org-beamer-export-to-pdf t))
;; (defun my-org-beamer-export-subtree-to-pdf ()
;;   (interactive)
;;   (org-beamer-export-to-pdf t t))
;;  (define-key org-mode-map (kbd "<f8>") 'org-beamer-export-to-pdf)
;; ;; (define-key org-mode-map (kbd "<S-f8>") 'org-beamer-export-to-latex)
;; ;; (define-key org-mode-map (kbd "s-<f8>") 'org-beamer-export-subtree-to-pdf)
;; 					; org-beamer-export-to-pdf

(defun djj/org-latex-export-subtree-to-pdf ()
  (interactive)
  (org-latex-export-to-pdf t t) )

(define-key org-mode-map  (kbd "<f8>" ) 'org-koma-letter-export-to-pdf)
(define-key org-mode-map  (kbd "<f7>" ) 'org-latex-export-to-pdf)
(define-key org-mode-map  (kbd "<f7>" ) 'djj/org-latex-export-subtree-to-pdf)
(define-key org-mode-map  (kbd "<f7>" ) 'org-publish-all)

(add-to-list 'ispell-skip-region-alist '("^#+BEGIN_SRC" . "^#+END_SRC"))

;; ;;(global-set-key (kbd "<f8>" ) 'org-latex-export-to-pdf)


;; (define-key org-mode-map (kbd "<f8>") 'org-latex-export-to-pdf)
;; (define-key org-mode-map (kbd "S-<f8>") 'org-koma-letter-export-to-pdf)
;; (define-key org-mode-map (kbd "s-<f8>") 'org-latex-export-subtree-to-pdf)
;; (define-key org-mode-map (kbd "<f7>") 'org-display-inline-images)
;; (define-key org-mode-map (kbd "s-<f7>") 'org-remove-inline-images)



;; ;;; * mu4e


;; ;; (add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")
;; ;; (require 'mu4e)

;; ;; (setq
;; ;;   mu4e-maildir       "~/Maildir"   ;; top-level Maildir
;; ;;   mu4e-sent-folder   "/sent"       ;; folder for sent messages
;; ;;   mu4e-drafts-folder "/drafts"     ;; unfinished messages
;; ;;   mu4e-trash-folder  "/trash"      ;; trashed messages
;; ;;   mu4e-refile-folder "/archive")

;; ;; ;;; **  update
;; ;; (setq
;; ;;   mu4e-get-mail-command "fetchmail"   ;; or fetchmail, or ...
;; ;;   mu4e-update-interval 300)             ;; update every 5 minutes



;; ;; ;;; ** error handling
;; ;; ;(setq mu4e-get-mail-command "fetchmail -v || [ $? -eq 1 ]")



;; ;; (setq send-mail-function    'smtpmail-send-it
;; ;;   smtpmail-stream-type  'ssl
;; ;;   smtpmail-smtp-service 465
;; ;;   smtpmail-debug-info t
;; ;;   smtpmail-debug-verb t)


;; ;; (defun roll-smtp ()
;; ;;   (interactive)
;; ;;   (setq smtpmail-smtp-server (car *myserver*))
;; ;;   (setq user-mail-address (car *mymail*))
;; ;;   (message "%s %s" user-mail-address smtpmail-smtp-server )
;; ;;   (setq *myserver*  (reverse *myserver*))
;; ;;   (setq *mymail*  (reverse *mymail*))
;; ;;   )

;; ;; ;; don't save messages to Sent Messages, Gmail/IMAP takes care of this
;; ;; ;;(setq mu4e-sent-messages-behavior 'delete)


;; ;; (add-hook 'mu4e-compose-mode-hook
;; ;;   (defun my-do-compose-stuff ()
;; ;;     "My settings for message composition."
;; ;;     (set-fill-column 72)
;; ;;     (flyspell-mode)))

;; ;; ;; don't keep message buffers around
;; ;; (setq message-kill-buffer-on-exit t)


;; ;; ;;; * notifications
;; ;; (mu4e-alert-set-default-style 'notifications)
;; ;; (add-hook 'after-init-hook #'mu4e-alert-enable-notifications)


;; ;; (setq mu4e-alert-interesting-mail-query
;; ;;       (concat
;; ;;        "flag:unread"
;; ;;        " AND NOT flag:trashed"
;; ;;        " AND NOT maildir:"
;; ;;        "\"/[Gmail].All Mail\""))




;; ;;; * log-edit-mode
(add-hook 'log-edit-mode-hook 'emojify-mode)
(add-hook 'vc-git-log-view-mode 'emojify-mode)

;;; * pabbrev
(require 'pabbrev)
(setq pabbrev-idle-timer-verbose nil
      pabbrev-read-only-error nil)
(define-key pabbrev-mode-map [tab] 'pabbrev-expand-maybe)
(add-hook 'text-mode-hook (lambda () (pabbrev-mode)))

(remove-hook 'text-mode-hook (lambda () (pabbrev-mode)))




 


;; * utilities




(defun transpose ()
  (interactive)
  (switch-to-buffer-other-window (get-buffer (buffer-name)))
  (switch-to-buffer (car (cdr (cdr (buffer-list))))))



;; (defun sudo-edit (&optional arg)
;;   "Edit currently visited file as root.

;; With a prefix ARG prompt for a file to visit.
;; Will also prompt for a file to visit if current
;; buffer is not visiting a file."
;;   (interactive "P")
;;   (if (or arg (not buffer-file-name))
;;       (find-file (concat "/sudo:root@localhost:"
;; 			 (ido-read-file-name "Find file(as root): ")))
;;     (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

;; * incubator


(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)


;; * shell-here
;;  (require 'shell-here)
;; (global-set-key (kbd "C-+") 'shell-here )


;;;; save in this directory
(defvar *paper* "~/Documents/paper/")
(defun mysave (filename dir)
  (interactive (list
                (read-from-minibuffer "filename: ")
                (read-from-minibuffer "dir: " (ido-read-buffer "~/Documents/papers/" ))
                ))
  (write-file (ido-read-directory-name "/home/djj/Documents/paper/" nil t nil)))




;; ** shift number up or down
(use-package shift-number
  :bind (("M-+" . 'shift-number-up)
	 ("M--" . 'shift-number-down)))


;; ;;; yankpad

(use-package yankpad
  :ensure t
  :defer 10
  :init
  (setq yankpad-file "~/yankpad.org")
  :config
  (bind-key "C-p" 'yankpad-map)
  (bind-key "C-s-p" 'yankpad-reload)
  (bind-key "C-o" 'yankpad-set-category)  
  ;; If you want to complete snippets using company-mode
  )



;; (eval-after-load 'ox '(require 'ox-koma-letter))






(defun align-& (start end)
  "align repeat \"& "
  (interactive "r")
  (align-regexp start end 
		(concat "\\(\\s-*\\)" "&") 1 1 t)
  )


(defun align-repeat (start end regexp)
  "Repeat alignment with respect to 
     the given regular expression."
  (interactive "r\nsAlign regexp: ")
  (align-regexp start end 
		(concat "\\(\\s-*\\)" regexp) 1 1 t))


;; (defun cquote ()
;;   (interactive)
;;   (while (re-search-forward "\\(^('\\w+','[^,]+\\),\\('[^)]+),\\)" nil t) (replace-match "\\1',\\2"))
;;   )


;; (setq scroll-down-aggressively 1.0)

(defun corral-double-quotes-backward-quick ()
 (interactive)
 (save-excursion
   (corral-double-quotes-backward)
   )
 (forward-char ))

(defun corral-single-quotes-backward-quick ()
 (interactive)
 (save-excursion
   (corral-single-quotes-backward)
   )
 (forward-char ))


;; ;; bifocal (interesting way to scroll but need customization
;; ;(require 'bifocal)
;; ;; coral
(require 'corral)

(global-set-key (kbd "M-)") 'corral-parentheses-backward)
(global-set-key (kbd "M-(") 'corral-parentheses-forward)
(global-set-key (kbd "M-]") 'corral-brackets-backward)
(global-set-key (kbd "M-[") 'corral-brackets-forward)
(global-set-key (kbd "M-{") 'corral-braces-backward)
(global-set-key (kbd "M-}") 'corral-braces-forward)
(global-set-key (kbd "M-|") 'corral-double-quotes-backward)
(global-set-key (kbd "M-}") 'corral-double-quotes-backward-quick)

(defvar ispell-tex-skip-alists
  '((("%\\[" . "%\\]")
     ;; All the standard LaTeX keywords from L. Lamport's guide:
     ;; \cite, \hspace, \hspace*, \hyphenation, \include, \includeonly, \input,
     ;; \label, \nocite, \rule (in ispell - rest included here)
     ("\\\\addcontentsline"              ispell-tex-arg-end 2)
     ("\\\\add\\(tocontents\\|vspace\\)" ispell-tex-arg-end)
     ("\\\\\\([aA]lph\\|arabic\\)"	 ispell-tex-arg-end)
     ("\\\\author"			 ispell-tex-arg-end)
     ("\\\\bibliographystyle"		 ispell-tex-arg-end)
     ("\\\\makebox"			 ispell-tex-arg-end 0)
     ;;("\\\\epsfig"		ispell-tex-arg-end)
     ("\\\\document\\(class\\|style\\)" .
      "\\\\begin[ \t\n]*{[ \t\n]*document[ \t\n]*}"))
    (;; delimited with \begin.  In ispell: displaymath, eqnarray, eqnarray*,
     ;; equation, minipage, picture, tabular, tabular* (ispell)
     ("\\(figure\\|table\\)\\*?"  ispell-tex-arg-end 0)
     ("list"			  ispell-tex-arg-end 2)
     ("program"		. "\\\\end[ \t\n]*{[ \t\n]*program[ \t\n]*}")
     ("verbatim\\*?"	. "\\\\end[ \t\n]*{[ \t\n]*verbatim\\*?[ \t\n]*}")
     ("gather\\*?"	. "\\\\end[ \t\n]*{[ \t\n]*gather\\*?[ \t\n]*}"))))




(defun my-quote (beginning end)
  "quote each element in region separated with SPACE and separate with COMMA"
  (interactive "*r")
  (setq end (copy-marker end))
  (save-match-data
    (save-excursion
      (goto-char beginning)
      (insert "\"")
      (while (re-search-forward " " end t)
        (replace-match "\",\"" nil nil) ) ;; delete the prefix
      (goto-char end)
      (insert "\""))))


(defun c-quote ()
  "quote each element in region separated with SPACE and separate with COMMA"
  (interactive)
  (let ((my-loc (point))
	(start (save-excursion  (progn (beginning-of-line) (point))))
	(point (search-backward "c " nil nil 1)))
    (forward-char 1)
    (insert "(")
    (insert "\"")
    (delete-char 1)
    (save-match-data
      (while  (re-search-forward " " my-loc t)
	(replace-match "\",\""  nil)
	(save-excursion (setq my-loc (+ 2 my-loc))))
      (goto-char (+ 1 my-loc))
      (insert "\")"))))


(defun c-mode-c-quote ()
  "quote each element in region separated with SPACE and separate with COMMA"
  (interactive)
  (let ((my-loc (point))
	(start (save-excursion  (progn (beginning-of-line) (point))))
	(point (search-backward "{ " nil nil 1)))
    (forward-char 1)
    (insert "\'")
    (delete-char 1)
    (save-match-data
      (while  (re-search-forward " " my-loc t)
	(replace-match "\', \'"  nil)
	(save-excursion (setq my-loc (+ 3 my-loc))))
      (goto-char (+ 1 my-loc))
      (delete-char 1)
      (insert "\', \'\\0\' }")))
  (back-to-indentation)
  (delete-char)
  )



;;; * password manager

(add-hook 'org-mode-hook 'org-password-manager-key-bindings)


					;  (setq_org-completion-use-ido_t)

(epa-file-enable)



(load "server")
(unless (server-running-p) (server-start))


(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-tags-exclude-from-inheritance (quote ("crypt")))
(setq org-crypt-key nil)
;; GPG key to use for encryption
;; Either the Key ID or set to nil to use symmetric encryption.

(setq auto-save-default nil)



;; ;; * hydra

;; ;; ** org-agenda mode

;; Hydra for org agenda (graciously taken from Spacemacs)
(defhydra hydra-org-agenda (:pre (setq which-key-inhibit t)
                                 :post (setq which-key-inhibit nil)
                                 :hint none)
  "
  Org agenda (quit)
                                                     
  Filter                 Headline              tools
  ^------^---------------  ^--------^-------    ^--------^------- 
  ft by tag              ht set status          l hl-line-mode
  fr refine by tag       hk kill         
  fc by category         r refile       
  fh by top headline     hA archive      
  fx by regexp           h: set tags     
  fd delete all filters  hp set priority 
  ^^                       ^^                
  ^^                       ^^                
  ^^                       ^^                
"
  ;; Headline
  ("hA" org-agenda-archive-default)
  ("hk" org-agenda-kill)
  ("r" org-agenda-refile)
  (":" org-agenda-set-tags)  
  ("ht" org-agenda-todo)
  ;; Filter
  ("fc" org-agenda-filter-by-category)
  ("fx" org-agenda-filter-by-regexp)
  ("ft" org-agenda-filter-by-tag)
  ("fr" org-agenda-filter-by-tag-refine)
  ("fh" org-agenda-filter-by-top-headline)
  ("fd" org-agenda-filter-remove-all)
  ;; tools
  ("l" hl-line-mode)
    ("q" quit-window "quit" :color blue))
;; (eval-after-load (org-agenda)
;;     (define-key org-agenda-mode-map  "." 'hydra-org-agenda/body))



;; ;;; * gnus


(setq compose-mail-user-agent-warnings nil)


(defun gnus-mail ()
  (interactive)
  (if (bufferp (get-buffer "*Group*"))
      (switch-to-buffer "*Group*")
    (gnus 1))) 

(global-set-key (kbd "<f5>") 'gnus-mail)


(setq gnus-inhibit-startup-message t)





;; ;;; * test

(defun set-margins (&optional int)
  (interactive "P")
  (unless int (setq int 10))
  "Set margins in current buffer."
  (setq left-margin-width int)
  (setq right-margin-width int))

;; * myfunction

(defun re-search-and-replace-in-file (file regex replacement)
  ""
  (interactive)
  (save-excursion
    (find-file-literally file)
    (goto-char  (buffer-end 0))
    (while (and (re-search-forward regex (buffer-end 1) t)
(> (point) 0))
(replace-match replacement))
(save-buffer)
(setq that-buffer (buffer-name))
(kill-buffer that-buffer) 
))

(defun re-search-and-insert-in-file (file regex insertion1 insertion2)
  (interactive)
  (save-excursion
    (find-file-literally file)
    (goto-char  (buffer-end 0))
    (while (and (re-search-forward regex (buffer-end 1) t)
		(> (point) 0))
      (forward-line)
      (insert insertion1)
      (forward-line)
      (insert insertion1)
      )
    (save-buffer)
    (setq that-buffer (buffer-name))
    (kill-buffer that-buffer) 
))

(defun re-search-and-get-from-file (file regex)
  (interactive)
  (save-excursion
    (find-file-literally file)
    (goto-char  (buffer-end 0))
    (re-search-forward regex (buffer-end 1) t 1)
    (condition-case nil
	(match-string 0)
      (error nil))    
    ))

(defun re-search-and-replace-in-buffer (regex replacement)
  (interactive)
  (save-excursion
    (goto-char  (buffer-end 0))
    (while (and (re-search-forward regex (buffer-end 1) t)
		(> (point) 0))
(replace-match replacement))
(save-buffer) 
))

;; ;; hydra switch-buffer



;; ;; (defun remove_nil_(x)
;; ;; (interactive)
;; ;; (number-to-string (replace-string "nil" x)))

;; ;; * drill
;; ;; (setq org-drill-save-buffers-after-drill-sessions-p nil)
 (require 'org-drill)

(defun drill ()
  (interactive)
  (save-excursion
    (find-file "/home/djj/Documents/org/drill/drill.org")
    (org-drill)))

(setq org-drill-learn-fraction 0.4)
(setq org-drill-save-buffers-after-drill-sessions-p nil)

(defun pdf-view--rotate (&optional counterclockwise-p page-p)
  "Rotate PDF 90 degrees.  Requires pdftk to work.\n
Clockwise rotation is the default; set COUNTERCLOCKWISE-P to
non-nil for the other direction.  Rotate the whole document by
default; set PAGE-P to non-nil to rotate only the current page.
\nWARNING: overwrites the original file, so be careful!"
  ;; error out when pdftk is not installed
  (if (null (executable-find "pdftk"))
      (error "Rotation requires pdftk")
    ;; only rotate in pdf-view-mode
    (when (eq major-mode 'pdf-view-mode)
      (let* ((rotate (if counterclockwise-p "left" "right"))
             (file   (format "\"%s\"" (pdf-view-buffer-file-name)))
             (page   (pdf-view-current-page))
             (pages  (cond ((not page-p)                        ; whole doc?
                            (format "1-end%s" rotate))
                           ((= page 1)                          ; first page?
                            (format "%d%s %d-end"
                                    page rotate (1+ page)))
                           ((= page (pdf-info-number-of-pages)) ; last page?
                            (format "1-%d %d%s"
                                    (1- page) page rotate))
                           (t                                   ; interior page?
                            (format "1-%d %d%s %d-end"
                                    (1- page) page rotate (1+ page))))))
        ;; empty string if it worked
        (if (string= "" (shell-command-to-string
                         (format (concat "pdftk %s cat %s "
                                         "output %s.NEW "
                                         "&& mv %s.NEW %s")
                                 file pages file file file)))
            (pdf-view-revert-buffer nil t)
          (error "Rotation error!"))))))

(defun pdf-view-rotate-clockwise (&optional arg)
  "Rotate PDF page 90 degrees clockwise.  With prefix ARG, rotate
entire document."
  (interactive "P")
  (pdf-view--rotate nil (not arg)))

(defun pdf-view-rotate-counterclockwise (&optional arg)
  "Rotate PDF page 90 degrees counterclockwise.  With prefix ARG,
rotate entire document."
  (interactive "P")
  (pdf-view--rotate :counterclockwise (not arg)))


;; ;;gscholar-bibtex=

;; ;; * bibtool + gscholar hacks


;; * browser

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "firefox")
(setq browse-url-browser-function `w3m-goto-url-new-session)
;; (key-chord-define-global "/'"  'w3m-goto-url ())



;;(add-to-list  'mm-inhibit-file-name-handlers 'openwith-file-handler)
;(openwith-mode t)
;(setq openwith-associations '(("\\.pdf\\'" "evince" (file))))
;(setq openwith-associations '(("\\.jpg\\'" "lximage-qt" (file))))
;(setq openwith-associations '(("\\.png\\'" "lximage-qt" (file))))
;(setq openwith-associations '(("\\.jpeg\\'" "lximage-qt" (file))))






;; (setq-local mytimer
;;     (run-with-idle-timer 2 t
;; 			 (lambda ()
;; 			   (when (file-newer-than-file-p "OrgFileName.org"
;; 							 "OrgFileName.tex")
;; 			     (org-export-dispatch "ll")
;; 			     (shell-command ".~/Documents/
;; ")
;; 			     ))))




;; (defun kill-region-regex (file regex-start regex-end)
;;   (interactive)
;;   (if (bufferp (get-file-buffer file))
;;       (progn (save-some-buffers  (get-file-buffer file ))	     
;;       (kill-buffer (get-file-buffer  file))))
;;   (find-file-literally file)
;;   (goto-char  (buffer-end 0))
;;   (setq begin (search-forward-regexp regex-start))
;;   (setq end (search-forward-regexp regex-end))
;;   (goto-char begin)
;;   (kill-region begin end)
;;   (save-buffer (current-buffer) )
;;   (kill-buffer (get-file-buffer  file))
;;   )



;; ;; some boost from Bastien
;; ;;https://github.com/bzg/dotemacs/blob/master/emacs.el


;; ;; Always use "y" for "yes"
(fset 'yes-or-no-p 'y-or-n-p)


;; ;; Stop polluting the directory with auto-saved files and backup
;; (setq auto-save-default nil)
;; (setq make-backup-files nil)
;; (setq auto-save-list-file-prefix nil)

;; ;; Well, it's more so that you know this option
;; (setq kill-whole-line t)
;; (setq kill-read-only-ok t)
;; (setq require-final-newline t)

;; ;; Scrolling done right
;; (setq scroll-error-top-bottom t)

;; ;; Number of lines of continuity when scrolling by screenfulls
;; (setq next-screen-context-lines 0)

;; (setq org-agenda-remove-tags t)
;; (setq org-agenda-restore-windows-after-quit t)
;; (setq org-agenda-show-inherited-tags nil)
;; (setq org-agenda-skip-deadline-if-done t)
;; (setq org-agenda-skip-deadline-prewarning-if-scheduled t)
 (setq org-agenda-skip-scheduled-if-done t)
 (setq org-agenda-skip-timestamp-if-done t)





;; ;; * google translate
;; ;; (global-set-key "\C-ct" 'google-translate-at-point)
;; ;; (global-set-key "\C-cT" 'google-translate-query-translate)


;; ;; * projectile
;; (setq projectile-indexing-method 'native)

;; (global-set-key (kbd "C-x f") 'projectile-find-file)
;; (use-package projectile
;;   :ensure t
;;   :config
;;   (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
;;   (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
;;   (projectile-mode +1))










;; ;; * my first emacs functions

;; (defun wrapPar ()
;;   "Wrap the line with ()"
;;   (interactive)
;;   (end-of-line)
;;   (insert ")")
;;   (beginning-of-line)
;;   (insert "(") 
;;     )

;; (global-set-key (kbd "C-\)") 'wrapPar)



;; ;; (defun paren2 ()
;; ;;   "Wrap word with parenthesis."
;; ;;   (interactive p)
;; ;;   (save-excursion
;; ;;     (insert ")")
;; ;;     (backward-word)
;; ;;      (insert "("))
;; ;;     (goto-char (+(point) 2)))
;; ;; (global-set-key (kbd "C-)") 'paren2)

;; ;; (defun alignlatex ()
;; ;;   " wrap line with & \\"
;; ;;   (interactive)
;; ;;   (save-excursion
;; ;;   (progn (back-to-indentation) (insert "&"))
;; ;;   (progn (end-of-line) (insert " \\\\"))
;; ;;   ))

;; ;; (global-set-key (kbd "C-s-q") 'alignlatex)

;; ;; (defun curly2 ()
;; ;;   "Wrap subscript with curly."
;; ;;   (interactive)
;; ;;   (save-excursion
;; ;;   (let ((beg (progn (back-to-indentation) (point)))
;; ;; 	(end (progn (end-of-line) (point))))	
;; ;;     (replace-regexp "\\(_\\|\\^\\)\\([\\+\\-]*\\w+[\\+\\-]*\\w*\\)" "\\1{\\2}" nil beg end)
;; ;;     ))
;; ;;   )
;; ;; (global-set-key (kbd "C-}") 'curly2)



;; ;; (defun paren2 ()
;; ;;   "Wrap word with parenthesis."
;; ;;   (interactive p)
;; ;;   (save-excursion
;; ;;     (insert ")")
;; ;;     (backward-word)
;; ;;      (insert "("))
;; ;;     (goto-char (+(point) 2)))
;; ;; (global-set-key (kbd "C-)") 'paren2)

;; (setq max-specpdl-size 20000)

;; (defun find-longest-word (max)
;;   (interactive "p")  
;;   (save-excursion
;;     (if (= (point) (buffer-end 1))
;; 	(progn (message "%s"  max) max)
;;       (setq this-column (progn (end-of-line) (current-column)))      
;;       (unless (> max this-column)
;; 	(setq max this-column))
;;       (forward-line)    
;;       (find-longest-word max)))
;;   )

;; ;; ** elisp mode

(autoload 'elisp-slime-nav-mode "elisp-slime-nav")
(add-hook 'emacs-lisp-mode-hook (lambda () (elisp-slime-nav-mode t)))
(add-hook 'emacs-lisp-mode-hook 'yas-minor-mode-on)

(defun  djj/insert-new-line (times)
  (interactive "p")
  (save-excursion
    (beginning-of-line)
    (newline times)))

(global-set-key (kbd "C-S-o") 'insert-new-line)

;; ;; **  elisp  test environment
(defun elisp-test ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (split-window-vertically)
  (find-file "/tmp/test.el")
  (find-file-other-window "/tmp/test.org")
  (windmove-right "test.org")
  (find-file  "/home/djj/Documents/org/programIdx.org"))

(defun c-test ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (split-window-vertically)
  (find-file "/home/djj/Documents/projects/programming/C/test.c")
  (find-file-other-window "/home/djj/Documents/ebooks/programming/programming_in_C.pdf")
  (windmove-right "test.org")
  (eshell))

(defun R-test ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (split-window-vertically)
  (find-file "/home/djj/Documents/projects/programming/R/test.R")
  (windmove-right "test.R")
  )




;; ;; * shell

;;   (defun alt-shell-dwim (arg)
;;    "Run an inferior shell like `shell'. If an inferior shell as its I/O
;;  through the current buffer, then pop the next buffer in `buffer-list'
;;  whose name is generated from the string \"*shell*\". When called with
;;  an argument, start a new inferior shell whose I/O will go to a buffer
;;  named after the string \"*shell*\" using `generate-new-buffer-name'."
;;    (interactive "P")
;;    (let* ((shell-buffer-list
;;  	  (let (blist)
;; 	     (dolist (buff (buffer-list) blist)
;; 	       (when (string-match "^\\*shell\\*" (buffer-name buff))
;; 	 	(setq blist (cons buff blist))))))
;; 	  (name (if arg
;; 	 	   (generate-new-buffer-name "*shell*")
;; 	 	 (car shell-buffer-list))))
;;      (shell name)))

;; (define-key shell-mode-map (kbd "<f7>") 'alt-shell-dwim)

;;(add-hook 'shell-mode 'pabbrev-mode)



;; ;; **  eshell
;; (require 'eshell)
;; (require 'em-smart)
;; ;(setq eshell-where-to-jump 'begin)
;; (setq eshell-review-quick-commands nil)
;; (setq eshell-smart-space-goes-to-end t)
;; (setenv "PATH"
;;   (concat
;;    "/home/djj/.local/bin/" ":"
;;    (getenv "PATH") ; inherited from OS
;;   )
;; )
;; ; eshell alias
;; ; cd /ssh:gnu:

;; (add-to-list 'load-path "~/Downloads/git/mode-line-stats")
;; ;; (require 'mode-line-stats)
;; ;; (mode-line-stats-mode)
;; ;; (setq mls-toggle-key (kbd "C-o"))
;; ;; (setq mls-modules '(battery cpu  memory))

;; ;; *
;; (setq multi-term-program "/bin/bash")
;; ;( term-unbind-key-list



;; ;; * utilities

(defun remove-from-kill-ring ()
    (interactive)
    (setq kill-ring (cdr kill-ring))
    )

(defun kill-region-no-ring (BEG END &optional REGION)
  (interactive (list (mark) (point) 'region))
  (kill-region BEG END  REGION)
  (pop kill-ring )
  (when kill-ring-yank-pointer
  (setq kill-ring-yank-pointer kill-ring))
  )

(defun kill-visual-line-backward ()
  (interactive)
  (kill-visual-line -1)
  )

(global-set-key (kbd "s-k") 'kill-visual-line-backward)
(global-set-key (kbd "s-w") 'kill-region-no-ring)


;; ;;(global-set-key (kbd "C-<backspace>") 'kill-region)


;; ;; * bbdb

(use-package bbdb-search
    :bind ("M-B" . bbdb)
    )


;; ;; (defun my-bbdb-create-record ()
;; ;;   "Prompt for and return a completely new BBDB record.
;; ;; Doesn't insert it in to the database or update the hashtables, but does
;; ;; ensure that there will not be name collisions."
;; ;;   (interactive)
;; ;;   (bbdb-records)                        ; make sure database is loaded
;; ;;   ;; (if bbdb-readonly-p
;; ;;   ;;     (error "The Insidious Big Brother Database is read-only."))
;; ;;   (let (firstname lastname net organization mail-alias)
;; ;;     (bbdb-error-retry
;; ;;      (progn
;; ;;        (let ((names (bbdb-divide-name (bbdb-read-string "Name: "))))
;; ;; 	 (setq firstname (car names)
;; ;; 	       lastname (nth 1 names)))
;; ;;        (when (string= firstname "")
;; ;; 	 (setq firstname nil))
;; ;;        (when (string= lastname "")
;; ;; 	 (setq lastname nil))))
;; ;;     (setq organization "gr5-2018")    
;; ;;     (setq net (bbdb-split (bbdb-read-string "mail: ") ","))    
;; ;;     ;(setq mail-alias (bbdb-read-string "mail-alias: "))
;; ;;     (setq mail-alias "proba2018")
;; ;;     (bbdb-create (vector (or firstname nil)
;; ;; 			 (or lastname nil)
;; ;; 			 nil nil nil
;; ;; 			 net
;; ;; 			 mail-alias
;; ;; 			 (make-vector bbdb-cache-length nil)))))

;; ;; (define-key bbdb-mode-map "c" 'my-bbdb-create-record)


(defun org-switch-to-agenda ()
    (interactive)
  (switch-to-buffer  "*Org Agenda*")
  )


;; ;; * unicode
;; ;; (require 'unipoint)
;; (defun put-dollar ()
;;   (interactive)
;;   (forward-sexp -1)
;;   (insert "$")
;;   (forward-sexp)	
;;   (insert "$")
;;   (forward-char)
;;   )


  

;; (key-chord-define-global "PO"     'switch-to-pdf)
;; (key-chord-define-global "TY"     'switch-to-tex)
;; (key-chord-define-global "r5"     'switch-to-R)
;; (key-chord-define-global "IO"     'switch-to-org)
;; (key-chord-define-global "*&"     'w3m)
;; (key-chord-define-global "hh"     '())
;; (key-chord-define-global [?h ?j]  'undo)  ; the same

;; (key-chord-define-global "jk"     'dabbrev-expand)
;; (key-chord-define-global "cv"     'reindent-then-newline-and-indent)
;; (key-chord-define-global "qo"     'goto-w3m-buffer)


;; (key-chord-define-global "qe"     'end-of-line)
;; (key-chord-define-global "qw"     'back-to-indentation)

(key-chord-define-global (kbd "0-") 'er/expand-region)
(key-chord-define-global (kbd ")_") 'er/contract-region)
(key-chord-define-global (kbd ";\'") 'ace-jump-mode)
(key-chord-define-global  (kbd ";[") 'ace-jump-char-mode)
(key-chord-define-global "./"     'goto-passwords)
(key-chord-define-global "qp"     'org-switch-to-agenda)
(key-chord-define-global "=-"     'eshell-toggle)





;; ;; *** key-chord thesis




;; ;; ** keychords orgmode
(key-chord-define org-mode-map  "nm"  'switch-to-R)

;; ;; ** keychords latex
;; ;(key-chord-define LaTeX-mode-map  ")("  '(yankpad-expand  "align"))
;; ;(key-chord-define LaTeX-mode-map  "qe"  'find-corresponding-other-window)






;; ;; ** c
;; ;(key-chord-define c-mode-map "»«"  'c-mode-c-quote)
 


;; ;; ## improve not yet
;; (key-chord-define inferior-ess-mode-map  "þþ"  '(ess-debug-set-error-action '(" r" "RECOVER" "utils::recover"))) 
;; (key-chord-define inferior-ess-mode-map  "þþ"  '(ess-debug-set-error-action '("" "NONE" "NULL")))


;; (key-chord-define ess-mode-map  "ää"  '(ess-debug-set-error-action '(" r" "RECOVER" "utils::recover")))
;; (key-chord-define ess-mode-map  "áá"  '(ess-debug-set-error-action '("" "NONE" "NULL")))




;;; key-chords passwords
(defun goto-passwords ()
  (interactive)
  (find-file-read-only "/home/djj/Documents/passwords/passwords.org.gpg"))



;; (defun find-corresponding-other-window ()
;;     (interactive)
;;   (let ((current-buffer (buffer-name)))
;;     (cond ((string-match "^test" current-buffer) (find-file-other-window (replace-regexp-in-string "test-" "" current-buffer)))
;; 	  ((string-match "^chap1" current-buffer) (find-file-other-window "/home/djj/Documents/projects/schoolquality/RealEstate/schoolquality/writing/sharelatex-version-control.tex"))
;; 	  ((string-match "^chap2" current-buffer) (find-file-other-window "/home/djj/Documents/projects/RepaymentRiskInMicrofinance/systemic-credit-risk/writing/version-control.tex"))
;; 	  ((string-match "^chap3" current-buffer) (find-file-other-window "/home/djj/Documents/projects/kiva/writing/version-control.tex"))
;; 	  (t (find-file-other-window (concat "test-" current-buffer))))))



;; * org-bullet

(require 'org-bullets)



;; * org present

(require 'org-present)
(autoload 'org-present "org-present" nil t)

(add-hook 'org-present-mode-hook
	  (lambda ()
	    (org-bullets-mode 1)            
	    (org-present-big)
	    (org-display-inline-images)
	    (define-key org-present-mode-keymap (kbd "C-<right>")  'org-present-next)
	    (define-key org-present-mode-keymap (kbd "C-<left>") 'org-present-prev)))

(add-hook 'org-present-mode-quit-hook
	  (lambda ()
	    (org-bullets-mode nil)
	    (org-present-small)	   
	    (org-remove-inline-images)))



(use-package org-tree-slide
   :ensure t
   :init
   (setq org-tree-slide-skip-outline-level 4)
   (org-tree-slide-simple-profile))


;; ;; * bbdv import csv

;; (defconst bbdb-csv-import-td
;;   '( (:namelist "Name")
;;     (:xfield "mail alias")
;;     (:mail "Primary Email" "Secondary Email")))


;; (defconst bbdb-csv-import-td
;;   '( (:namelist "First Name" "Last Name")
;;     (:mail-alias "Group")
;;     (:mail "Primary Email"))
;;   )


;; (defconst bbdb-csv-import-td
;;   '((:namelist "First Name" "Last Name")
;;     (:xfields "mail-alias")
;;     (:organization "Group")
;;     (:mail "Email")))

;; (setq bbdb-csv-import-mapping-table bbdb-csv-import-td)

;; (if (string-match-p "w3m" (buffer-name (car (buffer-list))))
;;     (message "T"))


;; ;; * w3m

;; ;;;; BUG cannot stop if no w3m buffer.
;; (defun check-if-w3m-in-buffer-in-list (buffer-list contp)
;;   (interactive)
;;   (if (eq (buffer-list) ())
;;       (message "no w3m")
;;       (if  (eq contp nil) 
;; 	  buffer-list
;; 	(if  (string-match-p "w3m" (buffer-name (car buffer-list)))
;; 	    (check-if-w3m-in-buffer-in-list  (car buffer-list) nil)
;; 	  (check-if-w3m-in-buffer-in-list (cdr buffer-list) t) )))
;;   )

;; ;; **  go to w3m
;; (defun goto-w3m-buffer ()
;;   (interactive)
;;   (switch-to-buffer 
;;    (get-buffer (check-if-w3m-in-buffer-in-list (buffer-list) t)))
;;   )

;; ;; ** switch between latex and html

;;   (setq org-babel-latex-htlatex "htlatex")
;;   (defmacro this-backend (&rest body)
;;     `(case org-export-current-backend ,@body))



;; ;;; highlight symbol
;; (global-set-key [(meta f3)] 'highlight-symbol-query-replace)
;; ;; (global-set-key [f3] 'highlight-symbol-next)
;; ;; (global-set-key [(shift f3)] 'highlight-symbol-prev)
;; ;; (global-set-key [(meta f3)] 'highlight-symbol-query-replace)


;; ;;; ** mime export customization

(setq org-html-mathjax-options '((path "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML")
				 (scale "100")
				 (align "left")
				 (font "TeX")
				 (linebreaks "false")
				 (autonumber "AMS")
				 (indent "0em")
				 (h "85%")
				 (tagindent ".8em")
				 (tagside "right")) )



;; (add-hook 'org-mime-html-hook
;;           (lambda ()
;;             (org-mime-change-element-style
;;              "pre" (format "color: %s; background-color: %s; padding: 0.5em;"
;;                            "#E6E1DC" "#232323"))))

;; (add-hook 'org-mime-html-hook
;;           (lambda ()
;;             (org-mime-change-element-style "p" (format "margin-right: 190px;"))))


;; ;; (add-hook 'org-mime-html-hook
;; ;;           (lambda ()
;; ;;             (org-mime-change-element-style
;; ;;              "blockquote" "border-left: 2px solid gray; padding-left: 4px;")))    


;; ;; (defun org-mime-change-class-style (class style)
;; ;;   "CLASS is used for new default html STYLE in exported html.")
;; ;; (while (re-search-forward (format "class=\"%s\"" class) nil t)
;; ;;   (replace-match (format "class=\"%s\" style=\"%s\"" class style)))

;; (add-hook 'org-mime-html-hook
;;           (lambda ()
;;             (org-mime-change-class-style
;;              "example" (format "margin-right: 150px;  margin-left: 150px; font-size: 12px"
;; 			       ) )))


;; (add-hook 'org-mime-html-hook
;;           (lambda ()
;;             (org-mime-change-class-style
;;              "org-src-container" (format "border: 1px solid black; margin-top: 10px ; margin-bottom: 10px; margin-right: 150px;  margin-left: 150px; background-color: white;foreground-color:black;font-size: 12px"
;; 					 ))))


;; ;; * gretl
;; (setq auto-mode-alist
;;       (cons '("\\.inp$" . gretl-mode) auto-mode-alist))
;; (autoload 'gretl-mode "gretl" nil t)
;; (add-hook 'gretl-mode-hook
;;           (lambda ()
;;             (abbrev-mode 1)
;;             (auto-fill-mode 1)
;;             (if (eq window-system 'x)
;;                 (font-lock-mode 1))))


;; (put 'upcase-region 'disabled nil)


;; ;;; * C

(add-hook 'c-mode-hook 'yas-minor-mode)
(define-key c-mode-map (kbd "M-}") 'corral-single-quotes-backward-quick)
(defun small-c-compile ()
  (interactive)
  (shell-command (concat "gcc " (buffer-name))))

(defun small-c-run ()
  (interactive)
  (shell-command "./a.out"))


(defun compile-and-run ()
  (interactive)
  (progn
    (small-c-compile)
    (small-c-run)))

;; (defun small-rcpp-compile ()
;;   (interactive)
;;   (shell-command (concat "R CMD SHLIB " (buffer-name))))


;; ;(define-key c++-mode-map (kbd "<C-f6>") 'small-rcpp-compile)

;; ;; (add-hook 'c-mode-hook 'paredit-everywhere-mode)
(add-hook 'c-mode-hook 'small-c-compile)
(add-hook 'c-mode-hook 'flycheck-mode)
(define-key c-mode-map (kbd "<C-f5>") 'small-c-compile)
(define-key c-mode-map (kbd "<f5>") 'small-c-run)





;; ;;; * shell

;; (defun new-shell ()
;;   (interactive)

;;   (let (
;;         (currentbuf (get-buffer-window (current-buffer)))
;;         (newbuf     (generate-new-buffer-name "*shell*"))
;;        )

;;    (generate-new-buffer newbuf)
;;    (set-window-dedicated-p currentbuf nil)
;;    (set-window-buffer currentbuf newbuf)
;;    (shell newbuf)
;;   )
;;   )

;; ;; ** shell to remote servers
;; (defun shell-to-flying ()
;;   (interactive)
;;   (let (
;;         (currentbuf (get-buffer-window (current-buffer)))
;;         (newbuf     (generate-new-buffer-name "*shell-flying*"))
;;        )
;;    (generate-new-buffer newbuf)
;;    (set-window-dedicated-p currentbuf nil)
;;    (set-window-buffer currentbuf newbuf)
;;    (shell newbuf)
;;    )
;;   (insert " ssh flyinggnu")
;;   (comint-send-input)
;;   )

;; (defun shell-to-biggnu ()
;;   (interactive)
;;   (let (
;;         (currentbuf (get-buffer-window (current-buffer)))
;;         (newbuf     (generate-new-buffer-name "*shell-biggnu*"))
;;        )

;;    (generate-new-buffer newbuf)
;;    (set-window-dedicated-p currentbuf nil)
;;    (set-window-buffer currentbuf newbuf)
;;    (shell newbuf)
;;    )
;;   (insert " ssh big")
;;   (comint-send-input)
;;   )


;; (defun shell-to-gnu ()
;;   (interactive)
;;   (let (
;;         (currentbuf (get-buffer-window (current-buffer)))
;;         (newbuf     (generate-new-buffer-name "*shell-gnu*"))
;;        )

;;    (generate-new-buffer newbuf)
;;    (set-window-dedicated-p currentbuf nil)
;;    (set-window-buffer currentbuf newbuf)
;;    (shell newbuf)
;;    )
;;   (insert " ssh lively")
;;   (comint-send-input)
;;   )




;; (defun send-myfun-to-herd ()
;;   (interactive)
;;   (shell-command  "scp /home/djj/Documents/projects/R/myfunctions.R lively:~/Documents/myfunctions.R")
;;   (shell-command  "scp /home/djj/Documents/projects/R/myfunctions.R big:~/Documents/myfunctions.R")
;;   (shell-command  "scp /home/djj/Documents/projects/R/myfunctions.R flying:~/Documents/myfunctions.R" )
;;   (shell-command  "scp /home/djj/Documents/projects/R/myfunctions.R weil:~/Documents/myfunctions.R"
;; 		  ))


;; (defun git-pull  ()
;;   (interactive)
;;   (make-comint "test" "ssh" nil  "-t" "lively" "cd /run/media/inner_drive/BigGnu1/kiva/;  git pull origin")
;;   (make-comint "test" "ssh" nil "-t" "big"  "cd /home/djj/Documents/projects/kiva/; git pull origin master")
;;   (make-comint "test" "ssh" nil "-t" "weil"  "cd /home/djj/Documents/projects/kiva/; git pull origin master")
;;   )


;; ;; * epub

;; (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

;; (setq nov-text-width most-positive-fixnum)
;; (setq visual-fill-column-center-text t)
;; (add-hook 'nov-mode-hook 'visual-line-mode)
;; (add-hook 'nov-mode-hook 'visual-fill-column-mode)

;; (defun my-nov-font-setup ()
;;   (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
;;                                            :height 1.0))
;; (add-hook 'nov-mode-hook 'my-nov-font-setup)


;; ;; * some cool function

;; (defun enclose (start end)
;;   (interactive "r")
;;   (goto-char start)
;;   (insert "{")
;;   (goto-char (+ 1 end))
;;   (insert "}"))




;; ;; * prettify symbol

(defun djj/prettify-symbols-default-compose-p (start end _match)
  "Return true iff the symbol MATCH should be composed.
The symbol starts at position START and ends at position END.
This is the default for `prettify-symbols-compose-predicate'
which is suitable for most programming languages such as C or Lisp."
  ;; Check that the chars should really be composed into a symbol.
  (let* ((syntaxes-beg (if (memq (char-syntax (char-after start)) '(?w ?_))
                           '(?w ?_) '(?. ?\\)))
         (syntaxes-end (if (memq (char-syntax (char-before end)) '(?w ?_))
                           '(?w ?_) '(?. ?\\))))
    (not (or (memq (char-syntax (or (char-before start) ?\s)) syntaxes-beg)
             (memq (char-syntax (or (char-after end) ?\s)) syntaxes-end)
             (nth 3 (syntax-ppss))))))

(setq prettify-symbols-compose-predicate 'djj/prettify-symbols-default-compose-p)


(defun greeks ()
(setq prettify-symbols-alist
      '(      
        ("alpha" . 945)    
        ("beta" . 946)    
        ("gamma" . 947)
	("delta" . 948)
	("epsilon" . 949)
	("zeta" . 950)
	("eta" . 951)
	("theta" . 952)
	("iota" . 953)
	("kappa" . 954)
	("lambda" . 955) ; λ
	("mu" . 956)
	("nu" . 957)
	("xi" . 958)
	("omicron" . 959)
	("pi" . 960)
	("rho" . 961)
	("sigma" . 963)
	("tau" . 964)
	("upsilon" . 965)
	("phi" . 966)
	("chi" . 967)
	("psi" . 968)
	("omega" . 969)
	("Omega" . 937)
	("Beta" . 914)
	("Sigma" . 931)
	("Lambda"  . 923)
	("Delta"  . 916)
	("Phi"  . 934 )
	("Pi" . 928)
	("._star" . 1645)
	("-lambda" . (?- (Br . Bl) 955))
	("ystar" . (?y (tr . Bc) 1645 (br . cr) ?i))
	("ystar1" . (?y (tr . Bc) 1645 (br . cr) ?1))
	("ystar2" . (?y (tr . Bc) 1645 (br . cr) ?2))
	("betahat" . (946 (cc . Bc) ?^))
	("beta_1" . (946 (br . Bl) ?1))	
	("beta_2" . (946 (br . Bl) ?2))
        ))
)




(global-prettify-symbols-mode 1)
(add-hook 'ess-r-mode-hook 'greeks)
(add-hook 'octave-mode-hook 'greeks)
(add-hook 'emacs-lisp-mode-hook 'greeks)
(add-hook 'ess-mode-hook 'greeks)
;(remove-hook 'LaTeX-mode-hook 'tex--prettify-symbols-alist)

(add-hook 'LaTeX-mode-hook 'greeks)


(defun r/get-begin-of-line-point ()
  (interactive)
  (save-excursion
    (beginning-of-line)
     (point)))


(defun r/eval-no-assign-r-fun-end ()
  "eval expression right of <-"
  (interactive)
  (setq begin (point))  
  (save-excursion     
    (re-search-backward "<- " (r/get-begin-of-line-point))
    (forward-char 3)
    (setq end (point)))
  (ess-eval-region begin end 4)
  )

(defun r/copy-left-of-assign-r-fun-end ()
  "kill-ring-save expression  left of <- and yank it at point"
  (interactive)
  (save-excursion     
    (re-search-backward "<- " (r/get-begin-of-line-point))
    (setq begin (point))
    (kill-ring-save begin (r/get-begin-of-line-point))
    )
  (yank)
  )

(defun djj/ess-select-function-call ()
  (interactive)
  (set-mark (re-search-forward ")" ))
  (progn (re-search-backward "(" )
	 (backward-word)))

 (define-key ess-mode-map (kbd "C-s-M-b") 'djj/ess-select-function-call)

;; ;; * test
;; ;(aset  auto-fill-chars  ?. t)


;; * LaTeX

(add-hook 'latex-mode-hook
	  (lambda ()
	    (font-lock-add-keywords nil
				    '(("\\(\\\\endnote\\)" 1
				       'font-lock-keyword-face t)
				      ("\\(\endnote\\)" 1
				       'font-lock-keyword-face t)))))

;; (defun tex-launcher ()
;;   (interactive)
;;   (shell-command "latexmk -pdf")
;;     )


;; ;; languagetool
;; (setq langtool-language-tool-jar "/home/djj/Downloads/git/languagetool/LanguageTool-4.5-stable/languagetool-commandline.jar")
;; (require 'langtool)


;;     (global-set-key "\C-x4w" 'langtool-check)
;;     (global-set-key "\C-x4W" 'langtool-check-done)
;;     (global-set-key "\C-x4l" 'langtool-switch-default-language)
;;     (global-set-key "\C-x44" 'langtool-show-message-at-point)
;;     (global-set-key "\C-x4c" 'langtool-correct-buffer)


;; (setq sentence-end-double-space nil)

(defun djj/latex/underscore (beg end)
  "replace X1 by X_1, \\beta_1 by \\beta_1 "
  (interactive "r")
  (goto-char beg)
  (while (and (re-search-forward "[0-9]" end t)
		(> (point) 0))
(replace-match "_\\&" nil nil))
  )


;; ;;
;; ;;; (defun )



;; (defun fill-sentence ()
;;   (interactive)
;;   (save-excursion
;;     (forward-sentence -1)
;;     (let ((beg (point)))
;;       (forward-sentence)
;;       (fill-region-as-paragraph beg (point)))))



;; ;; * org-publish
;; (require 'ox-publish)
;; (setq org-publish-project-alist
;;       '(
;; 	("org-notes"
;; 	 :base-directory "/home/djj/Documents/org/blog/"
;; 	 :base-extension "org"
;; 	 :publishing-directory "/home/djj/Documents/org/blog_html"
;; 	 :recursive t
;; 	 :publishing-function org-html-publish-to-html
;; 	 :headline-levels 4             ; Just the default for this project.
;; 	 :auto-preamble t
;; 	 )
;; 	("org-static"
;; 	 :base-directory "/home/djj/Documents/org/blog/"
;; 	 :base-extension "css\\|style\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
;; 	 :publishing-directory "/home/djj/Documents/org/blog_html"
;; 	 :recursive t
;; 	 :publishing-function org-publish-attachment
;; 	 )
;; 	("djj" :components ("org-notes" "org-static"))
;; 	))



(defun set-face-for-html ()
  (interactive)

  (face-spec-set
   'ess-constant-face
	 '((t :foreground "SteelBlue4"
	    ))
	 'face-defface-spec
	 )
  (face-spec-set
   'font-lock-string-face
   '((t :foreground "SteelBlue4"
      ))
   'face-defface-spec
   )
  (face-spec-set
   'ess-function-call-face
   '((t :foreground "MidnightBlue"
      ))
   'face-defface-spec
   )
  
  (face-spec-set
   'font-lock-comment-face
   '((t :foreground "DarkOliveGreen4"
      ))
   'face-defface-spec
   )
  
  (face-spec-set
   'ess-numbers-face
   '((t :foreground "SlateBlue4"
      ))
   'face-defface-spec
   )

  (face-spec-set 'ess-assignment-face
	 '((t :foreground "blue"
	    ))
	 'face-defface-spec	 
	 )

    (face-spec-set 'ess-operator-face
	 '((t :foreground "blue"
	    ))
	 'face-defface-spec	 
		 )

  )

(defun unset-face-for-html ()
  (interactive)
    (face-spec-set 'ess-assignment-face
	 '((t :foreground "Aquamarine"
	    ))
	 'face-defface-spec	 
		 )
    (face-spec-set
   'font-lock-comment-face
   '((t :foreground "#CDCD66660000"
      ))
   'face-defface-spec
   )
  
    (face-spec-set
	 'ess-constant-face
	 '((t :foreground "OliveDrab"
	    ))
	      'face-defface-spec
	 )
	 
    (face-spec-set
     'font-lock-string-face
     '((t :foreground "LightSalmon"
	))
     'face-defface-spec
     )
    
    (face-spec-set
     'ess-function-call-face
     '((t :foreground "LightSkyBlue"
	))
     'face-defface-spec
     )
    

    (face-spec-set
     'ess-numbers-face
     '((t :foreground "ivory"
	))
     'face-defface-spec
     )
      (face-spec-set 'ess-assignment-face
	 '((t :foreground "cyan"
	    ))
	 'face-defface-spec	 
	 )

    (face-spec-set 'ess-operator-face
	 '((t :foreground "cyan"
	    ))
	 'face-defface-spec	 
		 )

    )


(eval-after-load 'org '(require 'org-pdfview))
(eval-after-load ".org"
(progn (setq org-file-apps
    (quote
      ((auto-mode . emacs)
      ("\\.mm\\'" . default)
      ("\\.x?html?\\'" . (lambda (file link) (w3m link)))
      ("\\.pdf\\'" . (lambda (file link) (org-pdfview-open link))))))))

;; (setq org-export-current-backend 'latex)
;; ;(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; ;; latex git
;; ;; * new line after full stop in tex.
;; (defun my-electric-dot ()
;;   (interactive)
;;   (insert ".\n"))

;; (defun my-tex-hook ()
;;   (local-set-key (kbd ".") 'my-electric-dot))

;; ;(add-hook 'LaTeX-mode-hook 'my-tex-hook)
;; ;(remove-hook 'LaTeX-mode-hook 'my-tex-hook)
;; ;(remove-hook 'TeX-mode-hook 'my-tex-hook)


(defun djj-ess-narrow-to-defun ()
  (interactive)
  (save-excursion
    (progn (ess-mark-function-or-para)
	   (narrow-to-region (region-beginning) (region-end)))
    (deactivate-mark)  
  )) 

;; ; global-set-key (kbd "<home>") 'view-echo-area-messages)

;; (defun djj-dist-buffer ()
;;   (interactive)
;;   (let  ((current (current-buffer)))
;;     (if (> (length (window-list)) 1)
;; 	(switch-to-buffer-other-window  current)	
;;       )
;;     (previous-buffer)
;;     (previous-buffer) ))



;; (use-package org-chef
;;   :ensure t)

;; (require 'ob-async)
;; (setq ob-async-no-async-languages-alist '("ipython"))

;; ;(setq org-mode-map )





;; ;; (autoload 'ivy-bibtex "ivy-bibtex" "" t)
;; ;; ;; ivy-bibtex requires ivy's `ivy--regex-ignore-order` regex builder, which
;; ;; ;; ignores the order of regexp tokens when searching for matching candidates.
;; ;; ;; Add something like this to your init file:
;; ;; (setq ivy-re-builders-alist
;; ;;       '((ivy-bibtex . ivy--regex-ignore-order)
;; ;;         (t . ivy--regex-plus)))

(setq bibtex-completion-bibliography
      '("/home/djj/Documents/papers/mybibfile-formatted.bib"
        "/home/djj/Documents/papers/bibfile2-formatted.bib"
	))
(global-set-key (kbd "s-g") 'helm-bibtex)

(global-set-key (kbd "s-M-g") 'biblio-crossref-lookup)
(setq bibtex-completion-library-path '("/home/djj/Documents/papers/"))



;; ;; * Efficiency

;; (defun pretty (a b)
;;   (interactive "r")
;;   (save-restriction
;;     (narrow-to-region a b)
;;     (goto-char (point-min))
;;     (while (re-search-forward "\\([a-z]+\\)\s\\([a-z]*\\)" nil t) (replace-match "\\1_\\2" nil nil))
;;     (goto-char (point-min))
;;     (while (re-search-forward "_\\([a-z]*[\\+|\\-]*[0-9]+\\)" nil t) (replace-match "_{\\1}" nil nil))
;;     ))


;; (defun underscore (start end)
;;   (interactive "r")
;;   (save-restriction
;;     (narrow-to-region start end)
;;     (goto-char start)
;;     (while (re-search-forward "\s" nil t) (replace-match "_" nil nil)))
  
;;   )



;; (autoload 'git-blamed-mode "git-blamed"
;;   "Minor mode for incremental blame for Git." t)

;; ; magit-todos-keywords-list  ("HOLD" "TODO" "NEXT" "THEM" "PROG" "OKAY" "DONT" "FAIL" "KLUDGE" "HACK" "TEMP" "FIXME" "XXX+")
;; (require 'magit-todos)
;; (setq a '("a" "b"))
 (add-to-list 'magit-todos-keywords-list "WAITING IN-THE-END")
;(add-hook 'magit-mode-hook 'magit-todos-mode)
;;(setq magit-todos-exclude-globs "*.html\\|*.css\\|*.pdf")

;; Continuous clocking
(setq org-clock-continuously t)


;; (require 'ob-ipython)
;; (require 'tramp)


;; * shell
(defun test-shell-script ()
  (interactive)
  (with-current-buffer
    (find-file-noselect "/tmp/test.sh")
    (insert "#!/bin/bash\n")
    (save-buffer))
  (shell-command "sudo chmod +x /tmp/test.sh")
  (find-file "/tmp/test.sh")  
  (delete-other-windows )
  (split-window-horizontally)
  (eshell)
  (message "done"))

;;     (require 'auto-yasnippet)
;;     (global-set-key (kbd "s-\\") #'aya-create)
;;     (global-set-key (kbd "s-]") #'aya-expand)





(defun djj-tex-fill (start end)
  "Fill latex paragraphs leaving %comments untouched "
  (interactive "r")
  (narrow-to-region start end)
  (goto-char (point-min))
  (while  (re-search-forward "\\. " nil t) (replace-match ".%\n" nil nil))
  (LaTeX-fill-region-as-paragraph start end)
  (goto-char (point-min))
  (while  (re-search-forward "\\.%" nil t) (replace-match "." nil nil))
  (widen)  
  )
  



(defun put-in-images ()
  "append directory images to png files in latex
example given: \includegraphics[scale=0.4]{dist_number_stocks_per_company.png}
        expect: \includegraphics[scale=0.4]{images/dist_number_stocks_per_company.png\"
"
  (interactive)
  (while 
      (re-search-forward "\\({\\)\\(.*.png\\)" nil t)
    (replace-match "\\1images/\\2" )))



;;; * emacs-w3m
(setq w3m-key-binding 'info)
(eval-after-load "w3m"
  '(progn      (setq w3m-use-cookies t ;t
		     w3m-search-default-engine "duckduckgo"
		     w3m-user-agent "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)" 	   w3m-add-user-agent t
		     w3m-use-tab nil
		     w3m-use-title-buffer-name t
		     wget-download-directory "~/tmp"
		     wget-download-log-file "~/.emacs.d/wget-log.txt"
		     w3m-make-new-session t
		     w3m-fill-column 74)))

(eval-after-load "w3m-search"
  '(progn
     (setq w3m-search-engine-alist
           (append '(
                     ("wikipedia" "http://en.wikipedia.org/wiki/Special:Search?search=%s" nil)
		     ("google" "=%s" nil)
                     ("yt" "https://www.youtube.com/results?search_query=%s" nil)
                     ("leo_dict" "http://pda.leo.org/?lp=ende&lang=de&searchLoc=0&cmpType=relaxed&relink=on&sectHdr=off&spellToler=std&search=%s" nil)
                     ("ctan" "http://ctan.org/search/?phrase=%s" nil)
		     ("deb" "http://packages.debian.org/search?keywords=%s&searchon=names&suite=unstable&section=all" nil)
		     ("emacswiki" "https://duckduckgo.com/?q=%s++site%%3Aemacswiki.org&ia=web" nil)
                     ("duckduckgo" "https://duckduckgo.com/?q=%s" nil)
		     ("wikibooks" "https://en.wikibooks.org/w/index.php?search=%s" nil)
                     ("naver" "http://endic.naver.com/search.nhn?sLn=en&searchOption=all&query=%s")
		     ("arch" "https://www.mail-archive.com/search?l=all&q=%s")
		     ("microcapital" "https://duckduckgo.com/?q=%s++site%%3Amicrocapital.org&ia=web")
		     ("sch" "https://scholar.google.com/scholar?q=%s")
		     ("guardian" "https://duckduckgo.com/?q=%s++site%%3Ahttps://www.theguardian.com/")
		     ("daum" "http://dic.daum.net/search.do?q=%s&dic=eng#q")
		     ("wdictAa.aa" "https://en.wiktionary.org/wiki/%s")
		     ("dict" "http://www.dictionary.com/browse/%s?s=ts")
		     ("sci" "https://sci-hub.tw/%s")
		     ("libgen" "http://gen.lib.rus.ec/search.php?req=%s&lg_topic=libgen&open=0&view=simple&res=25&phrase=1&column=def")
		     ("ebay" "https://www.ebay.com/sch/i.html?_from=R40&_trksid=m570.l1313&_sacat=0&_nkw=%s")
		     ("osm" "https://nominatim.openstreetmap.org/search.php?q=%s+%s&polygon_geojson=1&viewbox=")))
	   )))

(eval-after-load "w3m-search"
  '(progn
     (setq w3m-uri-replace-alist
           (append '(("\\`w:" w3m-search-uri-replace "wikipedia")
                     ("\\`i:" w3m-search-uri-replace "imdb")
                     ("\\`l:" w3m-search-uri-replace "leo_dict")
                     ("\\`deb:" w3m-search-uri-replace "deb")
		     ("\\`sch:" w3m-search-uri-replace "sch")
                     ("\\`ewiki:" w3m-search-uri-replace "emacswiki")
		     ("\\`tex:" w3m-search-uri-replace "ctan")
                     ("\\`d:" w3m-search-uri-replace "duckduckgo")
		     ("\\`wikibooks:" w3m-search-uri-replace "latexwikibooks")
		     ("\\`google:" w3m-search-uri-replace "google")		     
		     ("\\`kr:" w3m-search-uri-replace "daum")
		     ("\\`arch:" w3m-search-uri-replace "arch")
		     ("\\`microcapital:" w3m-search-uri-replace "microcapital")
		     ("\\`dict:" w3m-search-uri-replace "dict")
		     ("\\`wdict:" w3m-search-uri-replace "wdict")
		     ("\\`sci:" w3m-search-uri-replace "sci")
		     ("\\`libgen:" w3m-search-uri-replace "libgen")
		     ("\\`ebay:" w3m-search-uri-replace "ebay")
		     ("\\`osm:" w3m-search-uri-replace "osm")
		     ("\\`yt:" w3m-search-uri-replace "yt")
                     )))))

(defun my-set-margins-for-w3m ()
  (interactive)
  "Set margins in current buffer."
  (setq left-margin-width 0)
  (setq right-margin-width 0))
(add-hook 'w3m-mode-hook (lambda () (my-set-margins-for-w3m)))

; (setq w3m-cookie-accept-domains "https://monucp.u-cergy.fr")
(require 'w3m)

(global-set-key (kbd "C-M-=") 'w3m-search)
(global-set-key (kbd "C-M-)") 'w3m-goto-url)

(eval-after-load "w3m-mode"
  (add-hook 'w3m-mode-hook 
	    (lambda () (w3m-lnum-mode))))
(define-key w3m-mode-map ";" 'opensx)


(defun opensx ()
  (interactive)
  (sx-open-link (w3m-print-current-url))
  )


;;(setq ess-help-pop-to-buffer t)




;; * Capture

(define-key global-map "\C-cq" 'org-capture)

(setq org-capture-templates
      '(("a" "agenda" entry (file+headline "~/Documents/refile.org" "Task of the day")
	 "* TODO %^{PROMPT} %^g \n%i\n%?\n%A\n" )
	("l" "linux" entry (file+headline "~/Documents/linux.org" "capture")
	 "* %^{PROMPT}\n%i\n\n%?\n\n%^g" )
	("p" "program index" entry (file+headline "/home/djj/Documents/org/programIdx.org" "program")
	 "* %^{PROMPT} %^g  \n%i\n\n%?\n\n%A\n " )
	("x" "expression" entry (file+headline "/home/djj/Documents/org/expressions.org" "expression")
	 "* %^{PROMPT} %^g  \n%i\n\n%?\n\n%A\n " )
	("r" "quick" entry (file "/home/djj/Documents/refile.org") 
	 "* TODO %c  \n %u \n" :immediate-finish t )	
	("t" "todo" entry (file "~/Documents/refile.org")
	 "* TODO %?\n%T\n%a\n" :clock-in t :clock-resume t)
	("h" "Habit" entry (file "~/Documents/refile.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n")
	
	("H" "Home" entry (file+headline"~/Documents/agenda.org" "Home")
	 "* TODO %^{PROMPT} %^g \n%i\n\n%?\n%A\n")
	("c" "Credit-Risk" entry (file+headline"~/Documents/agenda.org" "Systemic Risk")
	 "* TODO %^{PROMPT} %^g \n%i\n\n%?\n")
	("k" "korean" entry (file+headline"/home/djj/Documents/org/drill/drill.org" "drill")
	 "** Korean  :drill: \n %^{PROMPT}  \n*** Answer \n  %^{PROMPT}" )
	("K" "korean" entry (file+headline"/home/djj/Documents/org/drill/drill.org" "drill")
	 "** Korean  :drill: \n:PROPERTIES:\n:DRILL_CARD_TYPE: twosided \n:END:  \n %^{PROMPT}  \n*** Answer \n  %^{PROMPT}" )	
	("j" "job" entry (file+headline"~/Documents/jobs.org" "job")
	"** TODO  %^{PROMPT}\n 
         DEADLINE: %t
         :PROPERTIES:
         :University: %^{PROMPT}
         :LOCATION: %^{PROMPT}
         :Salary: %^{PROMPT} 
	 :Contract: %^{PROMPT}
         :DOC: %^{PROMPT}
	 :web: %A 
	 :END: \n\n" )
	))


;;; * pdf-tools

(defun switch-to-tex ()
  (interactive)
  (find-file (replace-regexp-in-string "\\.pdf" ".tex" (buffer-file-name)))
  )

(defun open-pdf ()
  (interactive)
  (async-shell-command (concat "evince " (buffer-file-name))))

(defun switch-to-org ()
  (interactive)
  (find-file  (concat (file-name-base buffer-file-name) ".org") )
  )


(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer t t))

(defun  get-pdf-file-name-from-bib-entry ()
  ; extract the first key entry in the file tmp-bibentry.bib
  (interactive)
  (save-excursion   
;; (shell-command "bibtool /tmp/tmp-bibentry.bib  -r keep_bibtex -r /home/djj/Documents/projects/shell-scripts/format-bibtex -o /tmp/tmp-bibentry.bib 
;; ")
    (find-file  "/tmp/tmp-bibentry.bib")
    (beginning-of-buffer)
    (re-search-forward  "^@.*,")
    (setq filename  (concat "~/Documents/papers/" (replace-regexp-in-string "@.*{\\s-*\\(.*\\)," "\\1"  (match-string 0)) ".pdf"))
    (kill-this-buffer)
    )
  ;(switch-to-prev-buffer)
  filename
  )

(defun save-paper ()
  "write pdf using bib-entry from /tmp/tmp-bibentry.bib"
    (interactive)
    (write-file (get-pdf-file-name-from-bib-entry)))

(use-package pdf-tools
  :magic ("%PDF" . pdf-view-mode)
  :bind (:map pdf-view-mode-map
	 ("s" . save-paper)
	 ("r" . revert-buffer-no-confirm)
	 ("O" . switch-to-org)
	 ("p" . open-pdf)
	 ("t" . switch-to-tex)
	 ("m" . pdf-view-midnight-minor-mode)
	 )
  :config
  (pdf-tools-install :no-query)
  (add-to-list 'pdf-tools-enabled-modes 'pdf-view-midnight-minor-mode)
  (add-to-list 'pdf-tools-enabled-modes 'auto-revert-mode)
  (setq pdf-view-midnight-colors '("white" . "black")))


(require 'doc-view)
(define-key doc-view-mode-map (kbd "m") 'pdf-view-mode)



(defun switch-to-R ()
  (interactive)
  (find-file (replace-regexp-in-string "\\..*$" ".R" (buffer-file-name)))
  )


(defun switch-to-pdf ()
  (interactive)
  (find-file (replace-regexp-in-string "\\..*$" ".pdf" (buffer-file-name))))



    (eval-after-load "tex"
      '(progn
	 (add-to-list 'TeX-expand-list
		      '("%(RubberPDF)"
			(lambda ()
			  (if
			      (not TeX-PDF-mode)
			      ""
	 (add-to-list 'TeX-command-list
		    '("Rubber" "rubber %(RubberPDF) %t --shell-escape --synctex" TeX-run-shell nil t) t)))
			 "--pdf --shell-escape --synctex"))))


    (defun rubber ()
      (interactive)
      (save-buffer)
      (if (equal (file-name-directory  (buffer-file-name)) "/home/djj/Documents/projects/thesis/")
	  (find-main)
	)  
      (let ((this-buffer (buffer-name)))   
	(TeX-run-shell nil (concat "rubber " " --pdf --synctex --shell-escape "  this-buffer) t)))
    (define-key LaTeX-mode-map (kbd "<f8>") 'rubber)

(use-package auctex
  :mode ("\\.tex\\'" . TeX-latex-mode)
  :config
  (defun latex-help-get-cmd-alist ()    ;corrected version:
    "Scoop up the commands in the index of the latex info manual.
   The values are saved in `latex-help-cmd-alist' for speed."
    ;; mm, does it contain any cached entries
    (if (not (assoc "\\begin" latex-help-cmd-alist))
        (save-window-excursion
          (setq latex-help-cmd-alist nil)
          (Info-goto-node (concat latex-help-file "Command Index"))
          (goto-char (point-max))
          (while (re-search-backward "^\\* \\(.+\\): *\\(.+\\)\\." nil t)
            (let ((key (buffer-substring (match-beginning 1) (match-end 1)))
                  (value (buffer-substring (match-beginning 2)
                                           (match-end 2))))
              (add-to-list 'latex-help-cmd-alist (cons key value))))))
    latex-help-cmd-alist)

  (add-hook 'TeX-after-compilation-finished-functions
	    #'TeX-revert-document-buffer)
)

(require 'latex)
(add-to-list 'auto-mode-alist '("\\.tex\\'" .) 'LaTeX-mode)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
;(add-hook 'LaTeX-mode-hook (lambda () TeX-fold-mode 1))
(add-hook 'latex-mode-hook 'visual-line-mode)
(add-hook 'tex-mode-hook 'visual-line-mode)
(add-hook 'Tex-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'outline-minor-mode)
(define-key LaTeX-mode-map (kbd "<C-tab>") 'outline-toggle-children)
(define-key LaTeX-mode-map (kbd "<f11>") 'open-pdf1)
(define-key LaTeX-mode-map (kbd "<f8>") 'rubber)
(define-key LaTeX-mode-map (kbd "å") 'hydra-tex/body)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)

(add-hook 'tex-mode-hook (function (lambda () (setq ispell-parser 'tex))))



;
;; (add-hook 'TeX-mode-hook
;;           '(lambda ()
;;             (define-key TeX-mode-map (kbd "<f9>")
;;               (lambda ()
;;                 (interactive)
;;                 (save-buffer)
;;                 (TeX-command-menu "Rubber")
;;                 (TeX-clean t)))
;;             (define-key TeX-mode-map (kbd "<C-f11>")
;;               (lambda ()
;;                 (interactive)
;;                 (TeX-view)
;;                 [return]))))



;;; * dtach

(setq R-remote-host "gnu")
(setq R-remote-host1 "blackpanther")
(setq R-remote-session "R-remote")
(setq R-remote-session1 "R-kr")
(setq R-remote-directory "/home/livelygnu/Documents/projects/creditRisk")
(setq R-remote-directory1 "/home/livelygnu/Documents/projects/schoolqly")
(setq R-remote-directory2 "/run/media/inner_drive/BigGnu1/kiva/coding")


(defun R-remote-lively ()
  "Connect to the remote-host's dtach session running R."
  (interactive)  
  (pop-to-buffer (make-comint (concat "remote-lively")
                              "ssh" nil "-t"  "-t" "lively" 
                              "cd ~/Documents" ";"
                              "dtach" "-A alive" 
                              "-z" "-E" "-r" "none"
                              inferior-R-program-name "--no-readline"))
  (ess-remote (process-name (get-buffer-process (current-buffer))) "R")
  (setq comint-process-echoes t))



;; don't know what it is
(use-package epa-file
  :config
  (epa-file-enable)
  )




;; * djj latex function

(defun latex/remove-tabular-env-from-tex-file (tex-file)
  "remove all tabular environment in tex a tex file"
    (if (string-match ".*\\.tex" tex-file)
	(re-search-and-replace-in-file tex-file "\\\\begin{tabular}.*\\S.*\\toprule\\|\\\\end{tabular}" "")))

(defun latex/remove-tabular-env-from-dir (dir) 
  (interactive)
  (if (eql (length dir) 0)
      (message "done")
    (progn (latex/remove-tabular-env-from-tex-file (car dir))
	  (latex/remove-tabular-env-from-dir (cdr dir)))))

(defun change-todo-done ()
  "change TODO: to DONE: in a tex"
  (interactive)
  (save-excursion
    (setq end (end-of-line))
    (beginning-of-line)
    (re-search-forward "TODO:" end t 1)
    (replace-match "DONE:")))


;; (use-package vterm
;;   :ensure t)  

;; * Racket

(add-hook 'racket-mode-hook (lambda () (add-function :before-until (local 'eldoc-documentation-function)
						#'racket-eldoc-function))
			      )
  
(add-hook 'racket-mode-hook      #'racket-unicode-input-method-enable)
(add-hook 'racket-repl-mode-hook #'racket-unicode-input-method-enable)


(defvar-local hidden-mode-line-mode nil)

(define-minor-mode hidden-mode-line-mode
  "Minor mode to hide the mode-line in the current buffer."
  :init-value nil
  :global t
  :variable hidden-mode-line-mode
  :group 'editing-basics
  (if hidden-mode-line-mode
      (setq hide-mode-line mode-line-format
            mode-line-format nil)
    (setq mode-line-format hide-mode-line
          hide-mode-line nil))
  (force-mode-line-update)
  ;; Apparently force-mode-line-update is not always enough to
  ;; redisplay the mode-line
  (redraw-display)
  (when (and (called-interactively-p 'interactive)
             hidden-mode-line-mode)
    (run-with-idle-timer
     0 nil 'message
     (concat "Hidden Mode Line Mode enabled.  "
             "Use M-x hidden-mode-line-mode to make the mode-line appear."))))


(setq org-format-latex-options '(:foreground  "White"  :background "Black" :scale 2.2
				 :html-foreground "Black"
				 :html-background "Transparent" :html-scale 1.1 
				 :matchers ("$1" "$" "$$" "\\(" "\\[")))


(eval-after-load 'ox-koma-letter
  '(progn
     (add-to-list 'org-latex-classes
                  '("my-letter"
                    "\\documentclass\{scrlttr2\}
     \\usepackage[english]{babel}
     \\setkomavar{frombank}{(1234)\\,567\\,890}
     \[DEFAULT-PACKAGES]
     \[PACKAGES]
     \[EXTRA]"))

     (setq org-koma-letter-default-class "my-letter")))



(defun format-data ()
  "format data to csv"
  (interactive)
  (while  (re-search-forward " +" nil t) (replace-match "," nil nil))
  )


(defun format-data-r ()
  "format data to csv"
  (interactive)
  (insert "read.table(text=\"")
  (while  (re-search-forward "[[:blank:]]+" nil t) (replace-match "," nil nil))
  (next-line)
  (insert "\",sep=',',header=TRUE) ")
  )







;; (setq ispell-aspell-data-dir "/usr/lib/aspell/")
;; (setq ispell-aspell-dict-dir ispell-aspell-data-dir)

(yas-global-mode 1)


(use-package js2  
  :mode ("\\.js\\'" . js2-mode)
  )
  
(add-hook 'js2-mode-hook 'skewer-mode)
(add-hook 'css-mode-hook 'skewer-css-mode)
(add-hook 'html-mode-hook '
	  skewer-html-mode)
(add-hook 'js2-mode-hook 'yas-minor-mode )

(require 'simple-httpd)
;; set root folder for httpd server
(setq httpd-root "/home/djj/Documents/projects/programming/js/battleship")


;; * publish


(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")


(setq website-html-head "")
(setq org-html-head-include-default-style nil)
(setq org-publish-project-alist
      '(
	("org-notes"
	 :base-directory "."
	 :base-extension "org"
	 :publishing-directory "./public/"	 
	 :recursive t
	 :publishing-function org-html-publish-to-html
	 :html-head-include-default-style nil
	 :html-head nil
	 :html-preamble nil
	 :headline-levels 4	  ; Just the default for this project.
	 :html-postamble ; replaces the auto-generated postamble
                  "<div class=\"clear-fix\"></div>
  <div id=\"doc-info\">
    Made with ♥ 
    using <a href=\"http://orgmode.org/\">Org-mode</a>
    and <a href=\"http://orgmode.org/worg/org-contrib/babel/\">Org-babel</a>.    
  </div>"
	 )
	("org-static"
	 :base-directory "."
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
	 :publishing-directory "./public/"
	 :recursive t
	 :exclude "public/"
	 :publishing-function org-publish-attachment
	 )
	("org" :components ("org-notes" "org-static"))
	))


;; org-mode org-babel replaces the current org mode window with code window when C-c '
(setq org-src-window-setup `current-window)


(require 'paredit)
(defun paredit-wrap-round-from-behind ()
  " sdfdsf | ==>(| sdfdsf)" 
  (interactive)
  (forward-sexp -1)
  (paredit-wrap-round)
  (insert " ")
  (forward-char -1))


(define-key paredit-mode-map (kbd "M-)")
  'paredit-wrap-round-from-behind)


;; * transparent

 (set-frame-parameter (selected-frame) 'alpha '(85 . 50))
 (add-to-list 'default-frame-alist '(alpha . (85 . 50)))
 (defun toggle-transparency ()
   (interactive)
   (let ((alpha (frame-parameter nil 'alpha)))
     (set-frame-parameter
      nil 'alpha
      (if (eql (cond ((numberp alpha) alpha)
                     ((numberp (cdr alpha)) (cdr alpha))
                     ;; Also handle undocumented (<active> <inactive>) form.
                     ((numberp (cadr alpha)) (cadr alpha)))
               100)
          '(85 . 50) '(100 . 100)))))
 (global-set-key (kbd "C-c t") 'toggle-transparency)


(use-package amx
  :after ivy
  :custom
  (amx-backend 'auto)
  (amx-save-file "~/.emacs.d/amx-items")
  (amx-history-length 50)
  (amx-show-key-bindings t)
  :config
  (amx-mode 1)
  )


;; disabled as it consume too much ressources and slow
;; it might be useful for some specific queries like C-h f
(use-package ivy-rich
  :norequire t
  :disabled t
  :config 
  (ivy-rich-mode 1))



;; *  async
;; (require 'async)

;; (use-package auth-source
;;   :no-require t
;;   :config (setq auth-sources '("~/.authinfo.gpg" "~/.netrc")))
	

;; (defun test-async-rubber ()
;;   (interactive)
;;   (async-start
;;    ;; What to do in the child process
;;    (lambda ()
;;      (sleep-for 3)
;;       )

;;    ;; What to do when it finishes
;;    (lambda (result)
;;      (message "%s" result))))

;; (global-set-key (kbd "C-<f6>") 'test-async-rubber)


;; (message "%s" "hello")
(add-hook 'magit-status-mode-hook 'visual-line-mode)
(add-hook 'magit-revision-mode-hook 'visual-line-mode)
;(add-hook 'magit-mode 'visual-line-mode)



;(require 'auto-yasnippet)
;
					;					;(global-set-key (kbd "C-<home>") #'aya-create)
					;(global-set-key (kbd "<home>") #'aya-expand)

;;(global-unset-key (kbd "C-<home>"))

(defun mc   (start end)
  (interactive "r")
  (re-search-forward "&" start end )
  (replace-match "&\\\\mc{1}{c}{"))


;; auto-complete
;; pcomplete

